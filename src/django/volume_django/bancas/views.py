from django.core.paginator import Paginator
from django.db.models import Count, Q
from django.http import JsonResponse
from lattes_data.models import Bancas, Pessoa


def index(request):
    """Index for bancas app.

   Parameters: a GET request with optionals 'ano_inicio', 'ano_fim', 'page' and 
   'page_size' parameters.

    Returns:
    JsonResponse with status code and a list of bancas.
   """

    ini_year = request.GET.get('ano_inicio', '2010')
    end_year = request.GET.get('ano_fim', '2019')
    page = request.GET.get('page', '1')
    page_size = request.GET.get('page_size', '15')

    if not ini_year.isdigit() or not end_year.isdigit():
        response_data = {
            'code': 404,
            'result': 'Ano inválido.'
        }
        return JsonResponse(response_data)

    if not page.isdigit() or not page_size.isdigit():
        response_data = {
            'code': 404,
            'result': 'Página ou tamanho de página inválido.'
        }
        return JsonResponse(response_data)

    ini_year, end_year = int(ini_year), int(end_year)
    result = []
    query_set = Bancas.objects.filter(
        ban_ano__gte=int(ini_year), ban_ano__lte=int(end_year)).order_by('-ban_ano')
    bancas_page = Paginator(query_set, page_size)

    for banca in bancas_page.page(page):
        participantes = []
        for participante in banca.participantes.all():
            participantes.append({
                "nome": participante.pe_nome_completo,
                "id_attes": participante.pe_id_lattes,
                "departamento": participante.pe_departamento
            })
        result.append({
            "ano": banca.ban_ano,
            "titulo": banca.ban_titulo,
            "aluno": banca.ban_aluno,
            "tipo": banca.ban_tipo_pesquisa,
            "participantes": participantes
        })

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)


def count(request):
    """Count for bancas app.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim', 
    'departamentos' and 'tipo' parameters.

    Returns:
    JsonResponse with status code and the count of bancas for each type, 
    department and year.
   """
    ini_year = request.GET.get('ano_inicio', '2010')
    end_year = request.GET.get('ano_fim', '2019')
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['ban_tipo_pesquisa']
                 for typ in Bancas.objects.values('ban_tipo_pesquisa').distinct()]

    if not ini_year.isdigit() or not end_year.isdigit():
        response_data = {
            'code': 404,
            'result': 'Ano inválido.'
        }
        return JsonResponse(response_data)

    ini_year, end_year = int(ini_year), int(end_year)

    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for department in departments:
        result[department] = {}
        for type in types:
            result[department][type] = {}
            parts_from_dep = Pessoa.objects.filter(
                pe_departamento__contains=department).values('pe_id_lattes')
            bancas = Bancas.objects.filter(
                ban_tipo_pesquisa=type, participantes__in=parts_from_dep).distinct()

            for l in labels:
                count = bancas.filter(ban_ano=l).aggregate(Count('ban_id'))
                result[department][type][l] = count['ban_id__count']

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)


def keywords(request):
    """Keywords for all bancas title.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the keywords with your specific frequency.
   """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return JsonResponse({"code": 404, "result": "Limite inválido."})

    if not limit > 0:
        return JsonResponse({"code": 404, "result": "Limite inválido."})

    keywords = Bancas.keywords(limit)
    result = []

    for keyword in keywords:
        result.append({keyword[0]: keyword[1]})

    return JsonResponse({'code': 200, 'result': result})


def map(request):
    """list of countries that participated most in bancas.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of countries and the specific 
    counter.
   """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return JsonResponse({"code": 404, "result": "Limite inválido."})

    if not limit > 0:
        return JsonResponse({"code": 404, "result": "Limite inválido."})

    countries = Bancas.objects.all().values(
        'ban_pais').annotate(total=Count('ban_pais')).order_by('-total')[:limit]
    result = []

    for country in countries:
        result.append({country['ban_pais']: country['total']})

    return JsonResponse({'code': 200, 'result': result})


def tipos(request):
    """List of bancas types.

    Returns:
    JsonResponse with a list of bancas types.
   """

    types = [typ['ban_tipo_pesquisa']
             for typ in Bancas.objects.values('ban_tipo_pesquisa').distinct()]

    return JsonResponse({'code': 200, 'result': types})


def count_tipos(request):
    """Count of all bancas per types.

    Returns:
    JsonResponse with the number of bancas for each type.
   """
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['ban_tipo_pesquisa']
                 for typ in Bancas.objects.values('ban_tipo_pesquisa').distinct()]

    result = {}
    for department in departments:
        result[department] = []
        for type in types:
            values = {}
            parts_from_dep = Pessoa.objects.filter(
                pe_departamento__contains=department).values('pe_id_lattes')
            bancas_count = Bancas.objects.filter(
                participantes__in=parts_from_dep).distinct().filter(ban_tipo_pesquisa=type).count()
            
            values[type] = bancas_count
            result[department].append(values)

    response_data = {
        "code": 200,
        "result": result,
    }
    return JsonResponse(response_data)

def rank(request):
    """Ranking of the people who most participated in bancas.

    Parameters: a GET request with optionals ano_inicio', 'ano_fim',
    'departamentos', 'tipos' and 'limit' parameters.

    Returns:
    JsonResponse with status code and a list of the <limit> docentes who most
    participated in bancas and the respective number of participations.
   """
    try:
        limit = int(request.GET.get('limit', 10))
    except ValueError:
        return JsonResponse({"code": 404, "result": "Limite inválido."})

    if not limit > 0:
        return JsonResponse({"code": 404, "result": "Limite inválido."})

    ini_year = request.GET.get('ano_inicio', '2010')
    end_year = request.GET.get('ano_fim', '2019')
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['ban_tipo_pesquisa']
                 for typ in Bancas.objects.values('ban_tipo_pesquisa').distinct()]

    if not ini_year.isdigit() or not end_year.isdigit():
        response_data = {
            'code': 404,
            'result': 'Ano inválido.'
        }
        return JsonResponse(response_data)

    ini_year, end_year = int(ini_year), int(end_year)

    ranking = Pessoa.objects\
        .filter(Q(pe_departamento__in=departments) &
                (Q(bancas__ban_tipo_pesquisa__in=types,
                   bancas__ban_ano__lte=int(end_year),
                   bancas__ban_ano__gte=int(ini_year)) |
                 Q(bancas=None)))\
        .annotate(bancas_count=Count('bancas'))\
        .order_by('-bancas_count')[:limit]

    result = []

    for value in ranking:
        result.append({
            "nome": value.pe_nome_completo,
            "numero_de_bancas": value.bancas_count
        })

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)
