from django.contrib import admin
from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /bancas/
    url(r'^$', views.index, name='bancas-index'),
    # ex: /bancas/count
    url(r'^count$', views.count, name='bancas-count'),
    # ex: /bancas/keywords
    url(r'^keywords$', views.keywords, name='bancas-keywords'),
    # ex: /bancas/map
    url(r'^map$', views.map, name='bancas-map'),
    # ex: /bancas/tipos
    url(r'^tipos$', views.tipos, name='bancas-tipos'),
    # ex: /bancas/count_tipos
    url(r'^count_tipos$', views.count_tipos, name='bancas-count_tipos'),
    # ex: /bancas/rank
    url(r'^rank$', views.rank, name='bancas-rank')
]
