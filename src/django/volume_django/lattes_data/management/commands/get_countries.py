from django.core.management.base import BaseCommand
import json
import requests
import pickle
from lattes_data.models import ProducaoArtistica, ProducaoBibliografica, ProducaoTecnica

class Command(BaseCommand):
    help = 'Searches geolocation info for all countries on the database'

    def handle(self, *args, **options):
        print("Iniciando execução do script ... ")

        print("Construindo dicionário auxiliar ...")
        aux_dict = {'Inglaterra': 'Reino Unido', 'Grã-Bretanha': 'Reino Unido',
                     'Escócia': 'Reino Unido', 'Samoa Americana': 'Estados Unidos',
                      'Cingapura': 'Singapura', 'Emirados Árabes': 'Emirados Árabes Unidos',
                      'Iugoslávia': 'Eslovênia', 'Vaticano': 'Itália', 'Hong Kong': 'China',
                      '': 'Brasil'}

        print("Construindo dicionário de país ...")
        country_dict = {}

        art_countries = ProducaoArtistica.objects.all().values('art_pais')
        bib_countries = ProducaoBibliografica.objects.all().values('bib_pais')
        tec_countries = ProducaoTecnica.objects.all().values('tec_pais')

        for country in art_countries:
            if not country['art_pais'] in country_dict:
                try:
                    info = json.loads(requests.get('https://nominatim.openstreetmap.org/search?country={}&format=json'.\
                    format(country['art_pais'])).text)[0]
                    country_dict[country['art_pais']] = {'lat': info['lat'], 'long': info['lon'], 'name':country['art_pais']}
                except:
                    try:
                        new_country = aux_dict[country['art_pais']]
                        info = json.loads(requests.get('https://nominatim.openstreetmap.org/search?country={}&format=json'.\
                        format(new_country)).text)[0]
                        country_dict[country['art_pais']] = {'lat': info['lat'], 'long': info['lon'], 'name':country['art_pais']}
                        #print("Não consigo achar o país {}".format(country['art_pais']))
                    except:
                        pass

        for country in bib_countries:
            if not country['bib_pais'] in country_dict:
                try:
                    info = json.loads(requests.get('https://nominatim.openstreetmap.org/search?country={}&format=json'.\
                    format(country['bib_pais'])).text)[0]
                    country_dict[country['bib_pais']] = {'lat': info['lat'], 'long': info['lon'], 'name':country['bib_pais']}
                except:
                    try:
                        new_country = aux_dict[country['bib_pais']]
                        info = json.loads(requests.get('https://nominatim.openstreetmap.org/search?country={}&format=json'.\
                        format(new_country)).text)[0]
                        country_dict[country['bib_pais']] = {'lat': info['lat'], 'long': info['lon'], 'name':country['bib_pais']}
                        #print("Não consigo achar o país {}".format(country['bib_pais']))
                    except:
                        pass

        for country in tec_countries:
            if not country['tec_pais'] in country_dict:
                try:
                    info = json.loads(requests.get('https://nominatim.openstreetmap.org/search?country={}&format=json'.\
                    format(country['tec_pais'])).text)[0]
                    country_dict[country['tec_pais']] = {'lat': info['lat'], 'long': info['lon'], 'name':country['tec_pais']}
                except:
                    try:
                        new_country = aux_dict[country['tec_pais']]
                        info = json.loads(requests.get('https://nominatim.openstreetmap.org/search?country={}&format=json'.\
                        format(new_country)).text)[0]
                        country_dict[country['tec_pais']] = {'lat': info['lat'], 'long': info['lon'], 'name':country['tec_pais']}
                        #print("Não consigo achar o país {}".format(country['tec_pais']))
                    except:
                        pass


        print("Salvando dictionário em util/countries.pickle ...")
        # Store data (serialize)
        with open('util/countries.pickle', 'wb') as handle:
            pickle.dump(country_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

        print("Script executado com sucesso!")
