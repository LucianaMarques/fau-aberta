from django.core.paginator import Paginator
from django.db.models import Count, Q
from django.http import JsonResponse
from util.format import make_chart_js
from lattes_data.models import Pessoa, Orientacao

def error(msg):
    response_data = {
        'code': 404,
        'result': msg,
    }

    return JsonResponse(response_data)
    
def index(request):
    """Index for orientacao route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim', 'page' and 
    'page_size' parameters.

    Returns:
    JsonResponse with status code and a list of premios for each year.
    """

    ini_year = request.GET.get('ano_inicio', '2010')
    end_year = request.GET.get('ano_fim', '2019')
    page = request.GET.get('page', '1')
    page_size = request.GET.get('page_size', '15')

    if not ini_year.isdigit() or not end_year.isdigit():
        return error('Ano Inválido')

    if not page.isdigit() or not page_size.isdigit():
        return error('Página ou tamanho de página inválido.')

    ini_year, end_year = int(ini_year), int(end_year)
    result = []
    query_set = Orientacao.objects.filter(
        ori_ano__gte=int(ini_year), ori_ano__lte=int(end_year)).order_by('-ori_ano')
    orientation_page = Paginator(query_set, page_size)

    for orientation in orientation_page.page(page):
        result.append({
            'id': orientation.ori_id,
            'pessoa_id': orientation.ori_pessoa_id,
            'aluno': orientation.ori_aluno,
            'ano': orientation.ori_ano,
            'titulo': orientation.ori_titulo,
            'pais': orientation.ori_pais,
            'instituicao': orientation.ori_instituicao,
            'agencia_fomento': orientation.ori_agencia_fomento,
            'tipo_orientacao': orientation.ori_tipo_orientacao,
            'tipo_pesquisa': orientation.ori_tipo_pesquisa,
        })

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)

def count(request):
    """Count for orientacao route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim', 
    'departamentos' and 'tipo' parameters.

    Returns:
    JsonResponse with status code and the count of orientations for 
    each type, department and year.
   """
    ini_year = request.GET.get('ano_inicio', '2010')
    end_year = request.GET.get('ano_fim', '2019')
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['ori_tipo_pesquisa']
                 for typ in Orientacao.objects.values('ori_tipo_pesquisa').distinct()]

    if not ini_year.isdigit() or not end_year.isdigit():
        return error('Ano Inválido')

    ini_year, end_year = int(ini_year), int(end_year)

    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for department in departments:
        result[department] = {}
        for type in types:
            result[department][type] = {}
            ori_from_dep = Pessoa.objects.filter(
                pe_departamento__contains=department).values('pe_id_lattes')
            ori = Orientacao.objects.filter(
                ori_tipo_pesquisa=type, ori_pessoa__in=ori_from_dep).distinct()
            for l in labels:
                count = ori.filter(ori_ano=l).aggregate(Count('ori_id'))
                result[department][type][l] = count['ori_id__count']

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)

def keywords(request):
    """Keywords for all orientations.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the keywords with your specific frequency.
   """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return error('Limite inválido')
    
    if not limit > 0:
        return error('Limite inválido')

    keywords = Orientacao.keywords(limit)
    result = []
    
    for keyword in keywords:
        result.append({keyword[0]: keyword[1]})
        
    return JsonResponse({'code': 200, 'result': result})

def map(request):
    """List of countries that oriented the most.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of countries and the specific 
    orientation counter.
   """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return error('Limite inválido')

    if not limit > 0:
        return error('Limite inválido')

    countries = Orientacao.objects.all().values(
        'ori_pais').annotate(total=Count('ori_pais')).order_by('-total')[:limit]
    result = []

    for country in countries:
        result.append({country['ori_pais']: country['total']})

    return JsonResponse({'code': 200, 'result': result})
    
def tipos(request):
    """List of orientations's types.

    Returns:
    JsonResponse with a list of orientations's types.
   """
    types = [typ['ori_tipo_pesquisa']
             for typ in Orientacao.objects.values('ori_tipo_pesquisa').distinct()]

    return JsonResponse({'code': 200, 'result': types})

def count_tipos(request):
    """Count of all orientation's per types.

    Returns:
    JsonResponse with the number of orientations for each type.
   """
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')
    
    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['ori_tipo_pesquisa']
                 for typ in Orientacao.objects.values('ori_tipo_pesquisa').distinct()]
        
    result = {}
    for department in departments:
        result[department] = []
        for type in types:
            values = {}
            ori_from_dep = Pessoa.objects.filter(
                pe_departamento__contains=department).values('pe_id_lattes')
            ori_count = Orientacao.objects.filter(
                ori_pessoa__in=ori_from_dep).distinct().filter(ori_tipo_pesquisa=type, ori_pessoa__in=ori_from_dep).count()
            values[type] = ori_count
            result[department].append(values)

    response_data = {
        "code": 200,
        "result": result,
    }
    return JsonResponse(response_data)

def rank(request):
    """Ranking of the people who oriented the most.

    Parameters: a GET request with optionals ano_inicio', 'ano_fim', 
    'departamentos', 'tipos' and 'limit' parameters.

    Returns:
    JsonResponse with status code and a list of the <limit> docentes who oriented
    the most.
   """

    try:
        limit = int(request.GET.get('limit', 10))
    except ValueError:
        return error('Limite inválido')

    if not limit > 0:
        return error('Limite inválido')
    
    ini_year = request.GET.get('ano_inicio', '2010')
    end_year = request.GET.get('ano_fim', '2019')
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['ori_tipo_pesquisa']
                 for typ in Orientacao.objects.values('ori_tipo_pesquisa').distinct()]

    if not ini_year.isdigit() or not end_year.isdigit():
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)

    result = []

    pessoas = Pessoa.objects.filter(pe_departamento__in=departments)

    for p in pessoas:
        orientacoes = p.orientacao_set.filter(ori_ano__lte=end_year, ori_ano__gte=ini_year).aggregate(Count('ori_id'))
        result.append((p.pe_nome_completo, orientacoes['ori_id__count']))

    result.sort(key=lambda tup: tup[1], reverse=True)

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)