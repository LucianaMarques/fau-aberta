from django.apps import AppConfig

class ProducaoArtisticaConfig(AppConfig):
    name = 'producao_artistica'
