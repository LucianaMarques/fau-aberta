import json
from django.test import TestCase, Client
from django.urls import reverse

from lattes_data.models import ProducaoArtistica, RelPessoaProdArt


class ViewProducaoArtisticaTests(TestCase):
    """ Test module for producao_artistica views """
    # fixture file
    fixtures = [
                'util/fixtures/docentes.json', 
                'producao_artistica/fixtures/tests.json'
            ]

    # initialize the APIClient app
    client = Client()
        
    def test_index_list_format_returns_all_elements(self):
        # get API response
        response = self.client.get(reverse('producao_artistica-index'), {'format': 'list'})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        # returns 5 elements from 2017 -> 2019
        self.assertEqual(len(producao_list), 5)

    def test_index_default_format_with_valid_year_range_returns_all_years(self):
        # get API response
        response = self.client.get(
            reverse('producao_artistica-index'), {'ano_inicio': 2017, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        # returns 3 keys: 2017, 2018 and 2019
        self.assertEqual(len(producao_list), 3)

    def test_index_default_format_with_valid_year_range_returns_correct_element(self):
        # get API response
        response = self.client.get(
            reverse('producao_artistica-index'), {'ano_inicio': 1998, 'ano_fim': 1998})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(producao_list), 1)
        self.assertEqual(list(producao_list.keys()), ['1998'])
        self.assertEqual(len(producao_list['1998']), 1)
    
    def test_index_default_format_for_specific_year_returns_all_elements(self):
        # get API response
        response = self.client.get(reverse('producao_artistica-index'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(producao_list['2015']), 2)  

    def test_index_invalid_year_range_returns_404(self):
        # get API response
        response = self.client.get(reverse('producao_artistica-index'), {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)
        
    def test_index_invalid_year_returns_404(self):
        # get API response
        response = self.client.get(reverse('producao_artistica-index'), {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)
    
    def test_index_invalid_format_returns_404(self):
        # get API response
        response = self.client.get(reverse('producao_artistica-index'), {'format': 'zebra'})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)
    
    def test_index_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()
        
        # get API response
        response = self.client.get(reverse('producao_artistica-index'), {'format': 'list'})
        json_response = json.loads(response.content)
        
        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(producao_list), 0)
        
    def test_count_returns_correctly_for_each_department(self):
        response = self.client.get(reverse('producao_artistica-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(prod_list), 3)

        for item in prod_list:
            assert item in ['AUH', 'AUP', 'AUT']
            self.assertEqual(len(prod_list[item]), 10)

    def test_count_returns_correctly_for_each_year(self):
        response = self.client.get(reverse('producao_artistica-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response 
        
        self.assertEqual(json_response['code'], 200)
        self.assertEqual(prod_list['AUH']['2011'], 1)
        self.assertEqual(prod_list['AUT']['2015'], 1)
        self.assertEqual(prod_list['AUH']['2015'], 1)
        self.assertEqual(prod_list['AUT']['2019'], 1)
        self.assertEqual(prod_list['AUH']['2019'], 1)
        self.assertEqual(prod_list['AUP']['2019'], 1)

    def test_count_returns_correctly_for_specific_year_range(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': 2017, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(prod_list), 3)

        for item in prod_list:
            assert item in ['AUH', 'AUP', 'AUT']
            self.assertEqual(len(prod_list[item]), 3)

    def test_count_returns_404_with_invalid_range(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_returns_404_with_invalid_year(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_returns_default_format(self):
        response1 = self.client.get(reverse('producao_artistica-count'))
        json_response1 = json.loads(response1.content)
        response2 = self.client.get(
            reverse('producao_artistica-count'), {'format': 'default'})
        json_response2 = json.loads(response2.content)
        self.assertEqual(json_response1, json_response2)

    def test_count_returns_404_with_invalid_format(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'format': 'zebra'})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_returns_correct_chartjs_format(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'format': 'chartjs'})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 200)

        result = json_response['result']
        assert 'datasets' in result
        assert 'labels' in result

        datasets = result['datasets']
        for item in datasets:
            assert 'label' in item
            assert 'data' in item
            for i in item:
                if i == 'data':
                    self.assertEqual(len(item[i]), 10)
                elif i == 'label':
                    assert item[i] in ['AUH', 'AUP', 'AUT']

        labels = result['labels']
        self.assertEqual(len(labels), 10)

    def test_count_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()

        response = self.client.get(reverse('producao_artistica-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(prod_list), 3)

        for item in prod_list:
            assert item in ['AUH', 'AUP', 'AUT']
            years = prod_list[item]
            for value in years:
                self.assertEqual(years[value], 0)
                
    def test_keywords_with_default_limit(self):
        response = self.client.get(reverse('producao_artistica-keywords'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        
        assert 'result' in json_response
        expected_result = [
            {"titulo": 6},
            {"teste": 3},
            {"foo": 2},
            {"bar": 1}
        ]
        self.assertEqual(json_response['result'], expected_result)
        
    def test_keywords_with_specific_limit(self):
        response = self.client.get(reverse('producao_artistica-keywords'), {"limit": 2})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        
        assert 'result' in json_response
        expected_result = [
            {"titulo": 6},
            {"teste": 3}
        ]
        self.assertEqual(json_response['result'], expected_result)
    
    def test_keywords_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()
        
        response = self.client.get(reverse('producao_artistica-keywords'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        
        assert 'result' in json_response
        self.assertEqual(json_response['result'], [])
        
    def test_keywords_with_negative_limit_returns_404(self):
        response = self.client.get(reverse('producao_artistica-keywords'), { "limit": -1 })
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)
    
    def test_keywords_with_invalid_limit_returns_404(self):
        response = self.client.get(
            reverse('producao_artistica-keywords'), {"limit": "foo"})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)
        
    def test_map_with_default_limit(self):
        response = self.client.get(reverse('producao_artistica-map'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        expected_result = [
            {'Brasil': {'lat': '-33.8689056', 'long': '5.2693306', 'total': 3}}, 
            {'Espanha': {'lat': '27.4335426', 'long': '43.9933088', 'total': 1}}, 
            {'Cazaquistão': {'lat': '40.5686476', 'long': '55.4421701', 'total': 1}}, 
            {'Austrália': {'lat': '-55.3228175', 'long': '-9.0880125', 'total': 1}}
        ]
        self.assertEqual(json_response['result'], expected_result)

    def test_map_with_specific_limit(self):
        response = self.client.get(
            reverse('producao_artistica-map'), {"limit": 2})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        expected_result = [
            {'Brasil': {'lat': '-33.8689056', 'long': '5.2693306', 'total': 3}}, 
            {'Espanha': {'lat': '27.4335426', 'long': '43.9933088', 'total': 1}}
        ]
        self.assertEqual(json_response['result'], expected_result)

    def test_map_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()

        response = self.client.get(reverse('producao_artistica-map'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        self.assertEqual(json_response['result'], [])

    def test_map_with_negative_limit_returns_404(self):
        response = self.client.get(
            reverse('producao_artistica-map'), {"limit": -1})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_map_with_invalid_limit_returns_404(self):
        response = self.client.get(
            reverse('producao_artistica-map'), {"limit": "foo"})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)
