from django.contrib import admin
from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /producao-artistica/
    url(r'^$', views.index, name='producao_artistica-index'),
    # ex: /producao-artistica/count
    url(r'^count$', views.count, name='producao_artistica-count'),
    # ex: /producao-artistica/keywords
    url(r'^keywords$', views.keywords, name='producao_artistica-keywords'),
    # ex: / producao-artistica/map
    url(r'^map$', views.map, name='producao_artistica-map'),
    # ex: / producao-artistica/tipos
    url(r'^tipos$', views.tipos, name='producao_artistica-tipos'),
    # ex: / producao-artistica/count_tipos
    url(r'^count_tipos$', views.count_tipos, name='producao_artistica-count_tipos'),
    # ex: / producao-artistica/rank
    url(r'^rank$', views.rank, name='producao_artistica-rank'),
]
