from django.apps import AppConfig

class ProducaoTecnicaConfig(AppConfig):
    name = 'producao_tecnica'
