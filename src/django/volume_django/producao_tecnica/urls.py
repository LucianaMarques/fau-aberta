from django.contrib import admin
from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /producao_tecnica/
    url(r'^$', views.index, name='producao_tecnica-index'),
    # ex: /producao_tecnica/count
    url(r'^count$', views.count, name='producao_tecnica-count'),
    # ex: /producao_tecnica/keywords
    url(r'^keywords$', views.keywords, name='producao_tecnica-keywords'),
    # ex: /producao_tecnica/map
    url(r'^map$', views.map, name='producao_tecnica-map'),
    # ex: /producao_tecnica/tipos
    url(r'^tipos$', views.tipos, name='producao_tecnica-tipos'),
    # ex: /producao_tecnica/count_tipos
    url(r'^count_tipos$', views.count_tipos, name='producao_tecnica-count_tipos'),
    # ex: /producao_tecnica/rank
    url(r'^rank$', views.rank, name='producao_tecnica-rank'),
]
