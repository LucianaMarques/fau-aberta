def parse_producao_tecnica(root, num_identificador):

    producoes_array = []

    for producoes in root.findall("PRODUCAO-TECNICA"):
        for producao in producoes:
            if producao.tag == "DEMAIS-TIPOS-DE-PRODUCAO-TECNICA":
                continue
            tipo = producao.tag
            artigo = "O"
            dados_producao = producao.find("DADOS-BASICOS-DO-{}".format(tipo))
            if dados_producao is None:
                artigo = "A"
                dados_producao = producao.find("DADOS-BASICOS-DA-{}".format(tipo))
            if dados_producao is None:
                artigo = "E"
                dados_producao = producao.find("DADOS-BASICOS-DE-{}".format(tipo))
            if dados_producao is None:
                print("Exception in parse_producao_tecnica: {} with {}".format(num_identificador, producao.tag))

            producoes_dict = {}
            producoes_dict['tec_id'] = ""
            producoes_dict['tec_autores'] = []
            producoes_dict['tec_tipo'] = producao.tag.replace("-", "_").lower()

            try:
                producoes_dict['tec_natureza'] = dados_producao.attrib['NATUREZA']
            except:
                producoes_dict['tec_natureza'] = ""

            producoes_dict['tec_titulo'] = ""
            try:
                producoes_dict['tec_titulo'] = dados_producao.attrib['TITULO-D{}-{}'.format(artigo, tipo)]
            except:
                pass
            if producoes_dict['tec_titulo'] == "":
                try:
                    producoes_dict['tec_titulo'] = dados_producao.attrib['TITULO-DO-PROCESSO']
                except:
                    pass
            if producoes_dict['tec_titulo'] == "":
                try:
                    producoes_dict['tec_titulo'] = dados_producao.attrib['TIPO-PRODUTO']
                except:
                    pass
            if producoes_dict['tec_titulo'] == "":
                try:
                    producoes_dict['tec_titulo'] = dados_producao.attrib['TITULO']
                except:
                    pass
            if producoes_dict['tec_titulo'] == "":
                print("Exception of titulo in producao_tecnica: {} with {}".format(tipo, dados_producao.attrib))

            try:
                producoes_dict['tec_ano'] = dados_producao.attrib['ANO']
            except:
                try:
                    producoes_dict['tec_ano'] = dados_producao.attrib['ANO-DESENVOLVIMENTO']
                except:
                    print("Exception of ano in producao_tecnica: {} with {}".format(tipo, dados_producao.attrib))

            producoes_dict['tec_pais'] = dados_producao.attrib['PAIS']


            producoes_array.append(producoes_dict)

    for producoes in root.findall("PRODUCAO-TECNICA/DEMAIS-TIPOS-DE-PRODUCAO-TECNICA"):
        for producao in producoes:

            tipo = producao.tag
            artigo = "O"
            dados_producao = producao.find("DADOS-BASICOS-DO-{}".format(tipo))
            if dados_producao is None:
                artigo = "A"
                dados_producao = producao.find("DADOS-BASICOS-DA-{}".format(tipo))
            if dados_producao is None:
                artigo = "E"
                dados_producao = producao.find("DADOS-BASICOS-DE-{}".format(tipo))
            if dados_producao is None:
                if tipo == "CURSO-DE-CURTA-DURACAO-MINISTRADO":
                    dados_producao = producao.find("DADOS-BASICOS-DE-CURSOS-CURTA-DURACAO-MINISTRADO")
                elif tipo == "DESENVOLVIMENTO-DE-MATERIAL-DIDATICO-OU-INSTRUCIONAL":
                    dados_producao = producao.find("DADOS-BASICOS-DO-MATERIAL-DIDATICO-OU-INSTRUCIONAL")

            if dados_producao is None:
                print("Exception in parse_producao_tecnica: {} with {}".format(num_identificador, producao.tag))

            producoes_dict = {}
            producoes_dict['tec_id'] = ""
            producoes_dict['tec_tipo'] = producao.tag.replace("-", "_").lower()
            producoes_dict['tec_pais'] = dados_producao.attrib['PAIS']
            producoes_dict['tec_ano'] = dados_producao.attrib['ANO']
            producoes_dict['tec_titulo'] = dados_producao.attrib['TITULO']

            autores = []
            producoes_dict['tec_autores'] = autores
            for autor in producao.findall("AUTORES"):
                autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
            producoes_dict['tec_autores'] = autores

            try:
                producoes_dict['tec_natureza'] = dados_producao.attrib['NATUREZA']
            except:
                producoes_dict['tec_natureza'] = ""

            producoes_array.append(producoes_dict)

    return producoes_array
