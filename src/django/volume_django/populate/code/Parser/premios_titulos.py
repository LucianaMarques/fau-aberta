def parse_premios_titulos(root, num_identificador):
    premios_titulos = root.find("DADOS-GERAIS/PREMIOS-TITULOS")
    if premios_titulos is None:
        return []

    pt_array = []
    for pt in premios_titulos:
        formacao_dict = {}
        formacao_dict['pre_id'] = ""
        formacao_dict['pre_pe_id'] = num_identificador
        formacao_dict['pre_nome'] = pt.attrib['NOME-DO-PREMIO-OU-TITULO'] # TODO WARN replace ' to \'
        formacao_dict['pre_ano'] = pt.attrib['ANO-DA-PREMIACAO']
        formacao_dict['pre_entidade'] = pt.attrib['NOME-DA-ENTIDADE-PROMOTORA']

        pt_array.append(formacao_dict)

    return pt_array
