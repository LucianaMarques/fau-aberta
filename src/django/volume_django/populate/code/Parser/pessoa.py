def parse_pessoa(root, department, num_identificador):
    pessoa = root.find("DADOS-GERAIS")
    endereco = root.find("DADOS-GERAIS/ENDERECO/ENDERECO-PROFISSIONAL")
    pessoa_dict = {}
    pessoa_dict['pe_id_lattes'] = num_identificador
    pessoa_dict['pe_nome_completo'] = pessoa.attrib['NOME-COMPLETO']
    pessoa_dict['pe_nome_citacao'] = pessoa.attrib['NOME-EM-CITACOES-BIBLIOGRAFICAS']
    pessoa_dict['pe_nacionalidade'] = pessoa.attrib['PAIS-DE-NASCIMENTO']
    pessoa_dict['pe_contato'] = endereco.attrib['TELEFONE']
    pessoa_dict['pe_departamento'] = department
    return pessoa_dict
