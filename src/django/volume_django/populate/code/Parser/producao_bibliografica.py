def parse_producao_bibliografica(root, num_identificador):

    producoes_array = []

    # TRABALHOS-EM-EVENTOS
    for producao in root.findall("PRODUCAO-BIBLIOGRAFICA/TRABALHOS-EM-EVENTOS/TRABALHO-EM-EVENTOS"):

        dados_artigo = producao.find("DADOS-BASICOS-DO-TRABALHO")
        detalhe_artigo = producao.find("DETALHAMENTO-DO-TRABALHO")

        producao_dict = {}
        producao_dict['bib_id'] = ""
        producao_dict['bib_tipo'] = "evento"
        producao_dict['bib_ano'] = dados_artigo.attrib['ANO-DO-TRABALHO']
        producao_dict['bib_natureza'] = dados_artigo.attrib['NATUREZA']
        producao_dict['bib_pais'] = dados_artigo.attrib['PAIS-DO-EVENTO']
        producao_dict['bib_titulo'] = dados_artigo.attrib['TITULO-DO-TRABALHO']
        autores = []
        for autor in producao.findall("AUTORES"):
            autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
        producao_dict['bib_autores'] = autores
        producao_dict['bib_volume'] = detalhe_artigo.attrib['VOLUME']
        paginas = 1
        try:
            i = int(detalhe_artigo.attrib['PAGINA-INICIAL'])
            f = int(detalhe_artigo.attrib['PAGINA-FINAL'])
            paginas = f - i
        except:
            paginas = 1
        producao_dict['bib_paginas'] = paginas
        producao_dict['bib_doi'] = dados_artigo.attrib['DOI']
        producao_dict['bib_publicacao'] = detalhe_artigo.attrib['TITULO-DOS-ANAIS-OU-PROCEEDINGS']
        producao_dict['bib_edicao'] = ""

        try:
            producao_dict['bib_volume'] = detalhe_artigo.attrib['VOLUME']
        except:
            producao_dict['bib_volume'] = ""
        try:
            producao_dict['bib_editora'] = detalhe_artigo.attrib['NOME-DA-EDITORA']
        except:
            producao_dict['bib_editora'] = ""

        producoes_array.append(producao_dict)

    # ARTIGOS-PUBLICADOS
    for producao in root.findall("PRODUCAO-BIBLIOGRAFICA/ARTIGOS-PUBLICADOS/ARTIGO-PUBLICADO"):

        dados_artigo = producao.find("DADOS-BASICOS-DO-ARTIGO")
        detalhe_artigo = producao.find("DETALHAMENTO-DO-ARTIGO")

        producao_dict = {}
        producao_dict['bib_id'] = ""
        producao_dict['bib_tipo'] = "artigo"
        producao_dict['bib_pais'] = dados_artigo.attrib['PAIS-DE-PUBLICACAO']
        producao_dict['bib_natureza'] = dados_artigo.attrib['NATUREZA']
        producao_dict['bib_ano'] = dados_artigo.attrib['ANO-DO-ARTIGO']
        producao_dict['bib_titulo'] = dados_artigo.attrib['TITULO-DO-ARTIGO']
        autores = []
        for autor in producao.findall("AUTORES"):
            autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
        producao_dict['bib_autores'] = autores
        producao_dict['bib_volume'] = detalhe_artigo.attrib['VOLUME']
        paginas = 1
        try:
            i = int(detalhe_artigo.attrib['PAGINA-INICIAL'])
            f = int(detalhe_artigo.attrib['PAGINA-FINAL'])
            paginas = f - i
        except:
            paginas = 1
        producao_dict['bib_paginas'] = paginas
        producao_dict['bib_doi'] = dados_artigo.attrib['DOI']
        producao_dict['bib_publicacao'] = detalhe_artigo.attrib['TITULO-DO-PERIODICO-OU-REVISTA']
        producao_dict['bib_volume'] = detalhe_artigo.attrib['VOLUME']
        producao_dict['bib_editora'] = ""
        producao_dict['bib_edicao'] = ""

        producoes_array.append(producao_dict)

    # Livros
    for producao in root.findall("PRODUCAO-BIBLIOGRAFICA/LIVROS-E-CAPITULOS/LIVROS-PUBLICADOS-OU-ORGANIZADOS/LIVRO-PUBLICADO-OU-ORGANIZADO"):

        dados_livro = producao.find("DADOS-BASICOS-DO-LIVRO")
        detalhe_livro = producao.find("DETALHAMENTO-DO-LIVRO")

        producao_dict = {}
        producao_dict['bib_id'] = ""
        producao_dict['bib_tipo'] = "livro"
        producao_dict['bib_ano'] = dados_livro.attrib['ANO']
        producao_dict['bib_natureza'] = dados_livro.attrib['NATUREZA']
        producao_dict['bib_pais'] = dados_livro.attrib['PAIS-DE-PUBLICACAO']
        producao_dict['bib_titulo'] = dados_livro.attrib['TITULO-DO-LIVRO']
        autores = []
        for autor in producao.findall("AUTORES"):
            autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
        producao_dict['bib_autores'] = autores
        producao_dict['bib_volume'] = detalhe_livro.attrib['NUMERO-DE-VOLUMES']
        producao_dict['bib_paginas'] = detalhe_livro.attrib['NUMERO-DE-PAGINAS']
        producao_dict['bib_edicao'] = detalhe_livro.attrib['NUMERO-DA-EDICAO-REVISAO']
        producao_dict['bib_doi'] = ""
        producao_dict['bib_publicacao'] = "Livro"

        try:
            producao_dict['bib_editora'] = detalhe_livro.attrib['NOME-DA-EDITORA']
        except:
            producao_dict['bib_editora'] = ""

        producoes_array.append(producao_dict)

    # Capitulos
    for producao in root.findall("PRODUCAO-BIBLIOGRAFICA/LIVROS-E-CAPITULOS/CAPITULOS-DE-LIVROS-PUBLICADOS/CAPITULO-DE-LIVRO-PUBLICADO"):

        dados_capitulo = producao.find("DADOS-BASICOS-DO-CAPITULO")
        detalhe_capitulo = producao.find("DETALHAMENTO-DO-CAPITULO")

        producao_dict = {}
        producao_dict['bib_id'] = ""
        producao_dict['bib_tipo'] = "capitulo"
        producao_dict['bib_ano'] = dados_capitulo.attrib['ANO']
        producao_dict['bib_natureza'] = ""
        producao_dict['bib_pais'] = dados_capitulo.attrib['PAIS-DE-PUBLICACAO']
        producao_dict['bib_titulo'] = dados_capitulo.attrib['TITULO-DO-CAPITULO-DO-LIVRO']
        autores = []
        for autor in producao.findall("AUTORES"):
            autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
        producao_dict['bib_autores'] = autores
        producao_dict['bib_volume'] = detalhe_capitulo.attrib['NUMERO-DE-VOLUMES']
        paginas = 1
        try:
            i = int(detalhe_capitulo.attrib['PAGINA-INICIAL'])
            f = int(detalhe_capitulo.attrib['PAGINA-FINAL'])
            paginas = f - i
        except:
            paginas = 1
        producao_dict['bib_doi'] = ""
        producao_dict['bib_publicacao'] = detalhe_capitulo.attrib['TITULO-DO-LIVRO']
        producao_dict['bib_paginas'] = paginas
        producao_dict['bib_edicao'] = detalhe_capitulo.attrib['NUMERO-DA-EDICAO-REVISAO']
        producao_dict['bib_editora'] = detalhe_capitulo.attrib['NOME-DA-EDITORA']

        producoes_array.append(producao_dict)

    # TEXTOS-EM-JORNAIS-OU-REVISTAS
    for producao in root.findall("PRODUCAO-BIBLIOGRAFICA/TEXTOS-EM-JORNAIS-OU-REVISTAS/TEXTO-EM-JORNAL-OU-REVISTA"):

        dados_jornal = producao.find("DADOS-BASICOS-DO-TEXTO")
        detalhe_jornal = producao.find("DETALHAMENTO-DO-TEXTO")

        producao_dict = {}
        producao_dict['bib_id'] = ""
        producao_dict['bib_tipo'] = "jornal"
        producao_dict['bib_natureza'] = dados_jornal.attrib['NATUREZA']
        producao_dict['bib_pais'] = dados_jornal.attrib['PAIS-DE-PUBLICACAO']
        producao_dict['bib_ano'] = dados_jornal.attrib['ANO-DO-TEXTO']
        producao_dict['bib_titulo'] = dados_jornal.attrib['TITULO-DO-TEXTO']
        autores = []
        for autor in producao.findall("AUTORES"):
            autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
        producao_dict['bib_autores'] = autores
        producao_dict['bib_volume'] = detalhe_jornal.attrib['VOLUME']
        paginas = 1
        try:
            i = int(detalhe_jornal.attrib['PAGINA-INICIAL'])
            f = int(detalhe_jornal.attrib['PAGINA-FINAL'])
            paginas = f - i
        except:
            paginas = 1
        producao_dict['bib_paginas'] = paginas
        try:
            producao_dict['data'] = parse_data(detalhe_jornal.attrib['DATA-DE-PUBLICACAO'])
        except:
            producao_dict['data'] = None
        producao_dict['bib_publicacao'] = detalhe_jornal.attrib['TITULO-DO-JORNAL-OU-REVISTA']
        producao_dict['bib_edicao'] = detalhe_jornal.attrib['TITULO-DO-JORNAL-OU-REVISTA'] + ", " + detalhe_jornal.attrib['TITULO-DO-JORNAL-OU-REVISTA']
        producao_dict['bib_editora'] = detalhe_jornal.attrib['TITULO-DO-JORNAL-OU-REVISTA']
        producao_dict['doi'] = ""

        try:
            producao_dict['bib_volume'] = detalhe_jornal.attrib['VOLUME']
        except:
            producao_dict['bib_volume'] = ""

        producoes_array.append(producao_dict)

    # DEMAIS-TIPOS-DE-PRODUCAO-BIBLIOGRAFICA
    for producoes in root.findall("PRODUCAO-BIBLIOGRAFICA/DEMAIS-TIPOS-DE-PRODUCAO-BIBLIOGRAFICA"):
        for producao in producoes:

            tipo = None
            dados_producao = None
            detalhe_producao = None

            if producao.tag == "OUTRA-PRODUCAO-BIBLIOGRAFICA":
                tipo = "OUTRA-PRODUCAO-BIBLIOGRAFICA"
                dados_producao = producao.find("DADOS-BASICOS-DE-OUTRA-PRODUCAO")
                detalhe_producao = producao.find("DETALHAMENTO-DE-OUTRA-PRODUCAO")
            else:
                tipo = producao.tag
                dados_producao = producao.find("DADOS-BASICOS-DO-{}".format(tipo))
                detalhe_producao = producao.find("DETALHAMENTO-DO-{}".format(tipo))
                if dados_producao is None:
                    dados_producao = producao.find("DADOS-BASICOS-DA-{}".format(tipo))
                    detalhe_producao = producao.find("DETALHAMENTO-DA-{}".format(tipo))
                if dados_producao is None:
                    dados_producao = producao.find("DADOS-BASICOS-DE-{}".format(tipo))
                    detalhe_producao = producao.find("DETALHAMENTO-DE-{}".format(tipo))
                if dados_producao is None:
                    print("Exception in producao_bibliografica: {} with tag {}".format(num_identificador, tipo))


            producao_dict = {}
            producao_dict['bib_id'] = ""
            producao_dict['bib_tipo'] = tipo.replace("-", "_").lower()
            producao_dict['bib_natureza'] = dados_producao.attrib['NATUREZA'].replace("-", "_").lower()
            producao_dict['bib_pais'] = dados_producao.attrib['PAIS-DE-PUBLICACAO']
            producao_dict['bib_ano'] = dados_producao.attrib['ANO']
            producao_dict['bib_titulo'] = dados_producao.attrib['TITULO']
            producao_dict['bib_doi'] = ""

            autores = []
            for autor in producao.findall("AUTORES"):
                autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
            producao_dict['bib_autores'] = autores

            producao_dict['bib_autores'] = autores

            try:
                producao_dict['bib_edicao'] = detalhe_producao.attrib['NUMERO-DA-EDICAO-REVISAO']
            except:
                producao_dict['bib_edicao'] = ""

            try:
                producao_dict['bib_volume'] = detalhe_producao.attrib['VOLUME']
            except:
                producao_dict['bib_volume'] = ""

            try:
                producao_dict['bib_editora'] = detalhe_producao.attrib['EDITORA']
            except:
                producao_dict['bib_editora'] = ""

            if producao_dict['bib_editora'] == "":
                try:
                    producao_dict['bib_editora'] = detalhe_producao.attrib['EDITORA-DA-TRADUCAO']
                except:
                    producao_dict['bib_editora'] = ""

            if producao_dict['bib_editora'] == "":
                try:
                    producao_dict['bib_editora'] = detalhe_producao.attrib['EDITORA-DO-PREFACIO-POSFACIO']
                except:
                    producao_dict['bib_editora'] = ""

            try:
                producao_dict['bib_publicacao'] = detalhe_producao.attrib['TITULO-DA-PUBLICACAO']
            except:
                producao_dict['bib_publicacao'] = ""

            if producao_dict['bib_publicacao'] == "":
                try:
                    producao_dict['bib_publicacao'] = detalhe_producao.attrib['TITULO-DA-OBRA-ORIGINAL']
                except:
                    producao_dict['bib_publicacao'] = ""

            producoes_array.append(producao_dict)

    return producoes_array