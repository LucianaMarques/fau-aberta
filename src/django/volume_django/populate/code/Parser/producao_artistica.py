def parse_producao_artistica(root, num_identificador):

    producoes_array = []

    for producoes in root.findall("OUTRA-PRODUCAO/PRODUCAO-ARTISTICA-CULTURAL"):
        for producao in producoes:
            tipo = producao.tag
            artigo = "O"
            dados_producao = producao.find("DADOS-BASICOS-DO-{}".format(tipo))
            if dados_producao is None:
                artigo = "A"
                dados_producao = producao.find("DADOS-BASICOS-DA-{}".format(tipo))
            if dados_producao is None:
                artigo = "E"
                dados_producao = producao.find("DADOS-BASICOS-DE-{}".format(tipo))
            if dados_producao is None:
                print("Exception in parse_producao_artistica: {} with {}".format(num_identificador, producao.tag))

            producoes_dict = {}
            producoes_dict['art_id'] = ""
            producoes_dict['art_tipo'] = producao.tag.replace("-", "_").lower()
            producoes_dict['art_natureza'] = dados_producao.attrib['NATUREZA']
            producoes_dict['art_titulo'] = dados_producao.attrib['TITULO']
            producoes_dict['art_pais'] = dados_producao.attrib['PAIS']
            producoes_dict['art_ano'] = dados_producao.attrib['ANO']

            autores = []
            for autor in producao.findall("AUTORES"):
                autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
            producoes_dict['art_autores'] = autores

            producoes_array.append(producoes_dict)
            
    return producoes_array
