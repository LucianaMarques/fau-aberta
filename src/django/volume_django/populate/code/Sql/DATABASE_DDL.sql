DROP TABLE IF EXISTS rel_pessoa_bancas CASCADE;
DROP TABLE IF EXISTS rel_pessoa_prod_tec CASCADE;
DROP TABLE IF EXISTS rel_pessoa_prod_art CASCADE;
DROP TABLE IF EXISTS rel_pessoa_prod_bib CASCADE;
DROP TABLE IF EXISTS rel_pessoa_participacao CASCADE;
DROP TABLE IF EXISTS bancas CASCADE;
DROP TABLE IF EXISTS orientacao CASCADE;
DROP TABLE IF EXISTS participacao CASCADE;
DROP TABLE IF EXISTS colaboradores CASCADE;
DROP TABLE IF EXISTS premios_titulos CASCADE;
DROP TABLE IF EXISTS producao_tecnica CASCADE;
DROP TABLE IF EXISTS producao_artistica CASCADE;
DROP TABLE IF EXISTS producao_bibliografica CASCADE;
DROP TABLE IF EXISTS pessoa CASCADE;

CREATE TABLE pessoa(
  pe_id_lattes          bigint,
  pe_nome_completo      text NOT NULL,
  pe_nome_citacao       text NOT NULL,
  pe_nacionalidade      text,
  pe_contato            text,
  pe_departamento       text NOT NULL,
  CONSTRAINT pk_pessoa PRIMARY KEY (pe_id_lattes)
);

CREATE TABLE premios_titulos(
  pre_id                SERIAL,
  pre_pe_id             bigint NOT NULL,
  pre_nome              text NOT NULL,
  pre_ano               int NOT NULL,
  pre_entidade          text,
  CONSTRAINT pk_premios_titulos PRIMARY KEY (pre_id),
  CONSTRAINT uniq_premios_titulos UNIQUE (pre_nome, pre_ano),
  CONSTRAINT fk_premios_titulos FOREIGN KEY (pre_pe_id)
    REFERENCES pessoa (pe_id_lattes)
);

CREATE TABLE orientacao(
  ori_id                SERIAL,
  ori_pessoa_id         bigint NOT NULL,
  ori_aluno             text NOT NULL,
  ori_ano               int NOT NULL,
  ori_titulo            text NOT NULL,
  ori_pais              text,
  ori_instituicao       text,
  ori_agencia_fomento   text,
  ori_tipo_orientacao   text,
  ori_tipo_pesquisa     text,
  CONSTRAINT pk_orientacao PRIMARY KEY (ori_id),
  CONSTRAINT uniq_orientacao UNIQUE (ori_pessoa_id, ori_aluno, ori_titulo, ori_ano),
  CONSTRAINT fk1_orientacao FOREIGN KEY (ori_pessoa_id)
    REFERENCES pessoa (pe_id_lattes)
);

CREATE TABLE bancas(
  ban_id                SERIAL,
  ban_aluno             text NOT NULL,
  ban_ano               int NOT NULL,
  ban_titulo            text NOT NULL,
  ban_pais              text,
  ban_instituicao       text,
  ban_tipo_pesquisa     text,
  CONSTRAINT pk_bancas PRIMARY KEY (ban_id),
  CONSTRAINT uniq_bancas UNIQUE (ban_aluno, ban_ano, ban_titulo)
);

CREATE TABLE rel_pessoa_bancas(
  pe_ban_id             SERIAL,
  pe_ban_pessoa_id      bigint NOT NULL,
  pe_ban_bancas_id      bigint NOT NULL,
  CONSTRAINT pk_rel_pessoa_bancas PRIMARY KEY (pe_ban_id),
  CONSTRAINT uniq_rel_pessoa_bancas UNIQUE (pe_ban_pessoa_id, pe_ban_bancas_id),
  CONSTRAINT fk1_rel_pessoa_bancas FOREIGN KEY (pe_ban_pessoa_id)
    REFERENCES pessoa (pe_id_lattes),
  CONSTRAINT fk2_rel_pessoa_bancas FOREIGN KEY (pe_ban_bancas_id)
    REFERENCES bancas (ban_id)
);

CREATE TABLE producao_tecnica(
  tec_id                SERIAL,
  tec_tipo              text NOT NULL,
  tec_titulo            text NOT NULL,
  tec_ano               int NOT NULL,
  tec_autores           text,
  tec_natureza          text,
  tec_pais              text,
  CONSTRAINT pk_producao_tecnica PRIMARY KEY (tec_id),
  CONSTRAINT uniq_producao_tecnica UNIQUE (tec_tipo, tec_titulo, tec_ano)
);

CREATE TABLE rel_pessoa_prod_tec(
  pe_tec_id             SERIAL,
  pe_tec_pessoa_id      bigint NOT NULL,
  pe_tec_prod_tec_id    bigint NOT NULL,
  CONSTRAINT pk_rel_pessoa_prod_tec PRIMARY KEY (pe_tec_id),
  CONSTRAINT uniq_rel_pessoa_prod_tec UNIQUE (pe_tec_pessoa_id, pe_tec_prod_tec_id),
  CONSTRAINT fk1_rel_pessoa_prod_tec FOREIGN KEY (pe_tec_pessoa_id)
    REFERENCES pessoa (pe_id_lattes),
  CONSTRAINT fk2_rel_pessoa_prod_tec FOREIGN KEY (pe_tec_prod_tec_id)
    REFERENCES producao_tecnica (tec_id)
);

CREATE TABLE producao_artistica(
  art_id                SERIAL,
  art_tipo              text NOT NULL,
  art_titulo            text NOT NULL,
  art_ano               int NOT NULL,
  art_autores           text,
  art_natureza          text,
  art_pais              text,
  CONSTRAINT pk_producao_artistica PRIMARY KEY (art_id),
  CONSTRAINT uniq_producao_artistica UNIQUE (art_tipo, art_titulo, art_ano)
);

CREATE TABLE rel_pessoa_prod_art(
  pe_art_id             SERIAL,
  pe_art_pessoa_id      bigint NOT NULL,
  pe_art_prod_art_id    bigint NOT NULL,
  CONSTRAINT pk_rel_pessoa_prod_art PRIMARY KEY (pe_art_id),
  CONSTRAINT uniq_rel_pessoa_prod_art UNIQUE (pe_art_pessoa_id, pe_art_prod_art_id),
  CONSTRAINT fk1_rel_pessoa_prod_art FOREIGN KEY (pe_art_pessoa_id)
    REFERENCES pessoa (pe_id_lattes),
  CONSTRAINT fk2_rel_pessoa_prod_art FOREIGN KEY (pe_art_prod_art_id)
    REFERENCES producao_artistica (art_id)
);

CREATE TABLE producao_bibliografica(
  bib_id                SERIAL,
  bib_tipo              text NOT NULL,
  bib_titulo            text NOT NULL,
  bib_ano               int NOT NULL,
  bib_autores           text,
  bib_publicacao        text,
  bib_editora           text,
  bib_edicao            text,
  bib_volume            text,
  bib_natureza          text,
  bib_pais              text,
  CONSTRAINT pk_producao_bibliografica PRIMARY KEY (bib_id),
  CONSTRAINT uniq_producao_bibliografica UNIQUE (bib_tipo, bib_titulo, bib_ano)
);

CREATE TABLE rel_pessoa_prod_bib(
  pe_bib_id             SERIAL,
  pe_bib_pessoa_id      bigint NOT NULL,
  pe_bib_prod_bib_id    bigint NOT NULL,
  CONSTRAINT pk_rel_pessoa_prod_bib PRIMARY KEY (pe_bib_id),
  CONSTRAINT uniq_rel_pessoa_prod_bib UNIQUE (pe_bib_pessoa_id, pe_bib_prod_bib_id),
  CONSTRAINT fk1_rel_pessoa_prod_bib FOREIGN KEY (pe_bib_pessoa_id)
    REFERENCES pessoa (pe_id_lattes),
  CONSTRAINT fk2_rel_pessoa_prod_bib FOREIGN KEY (pe_bib_prod_bib_id)
    REFERENCES producao_bibliografica (bib_id)
);

CREATE TABLE participacao(
  par_id                SERIAL,
  par_nome_ev           text NOT NULL,
  par_tipo              text NOT NULL,
  par_ano               int NOT NULL,
  par_natureza          text,
  par_pais              text,

  CONSTRAINT pk_participacao PRIMARY KEY (par_id),
  CONSTRAINT uniq_participacao UNIQUE (par_tipo, par_nome_ev, par_ano)
);

CREATE TABLE rel_pessoa_participacao(
  pe_par_id               SERIAL,
  pe_par_pessoa_id        bigint NOT NULL,
  pe_par_participacao_id  bigint NOT NULL,
  pe_par_titulo           text NOT NULL,
  pe_par_forma_par        text,
  pe_par_tipo_par         text,

  CONSTRAINT pk_rel_pessoa_participacao PRIMARY KEY (pe_par_id),
  CONSTRAINT uniq_rel_pessoa_participacao UNIQUE (pe_par_pessoa_id, pe_par_participacao_id, pe_par_titulo),
  CONSTRAINT fk1_rel_pessoa_participacao FOREIGN KEY (pe_par_pessoa_id)
    REFERENCES pessoa (pe_id_lattes),
  CONSTRAINT fk2_rel_pessoa_participacao FOREIGN KEY (pe_par_participacao_id)
    REFERENCES participacao (par_id)
);

CREATE TABLE colaboradores(
  co_id                         SERIAL,
  co_pe_id1                     bigint NOT NULL,
  co_pe_id2                     bigint NOT NULL,
  CONSTRAINT pk_colaboradores PRIMARY KEY (co_id),
  CONSTRAINT uniq_colaboradores UNIQUE (co_pe_id1, co_pe_id2),
  CONSTRAINT self_colaboradores CHECK (co_pe_id1 != co_pe_id2)
);
