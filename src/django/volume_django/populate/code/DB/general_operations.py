import psycopg2
from .connection import *

# Makes an select in pessoa table
def select_id_pessoa(attr, conn = None, cur = None):
    """
    Selects in pessoa.

    The select_id_pessoa function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the pessoa table
    :type ev: dict of (<class 'bigint'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT pe_id_lattes FROM pessoa WHERE pe_id_lattes = %s", [attr["pe_id_lattes"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in pessoa table
def insert_pessoa(attr, conn = None, cur = None):
    """
    Inserts in pessoa.

    The insert_pessoa function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the pessoa table
    :type ev: dict of (<class 'bigint'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO pessoa (pe_id_lattes, pe_nome_completo, pe_nome_citacao, pe_nacionalidade, pe_contato, pe_departamento) VALUES (%s, %s, %s, %s, %s, %s)', [attr["pe_id_lattes"], attr["pe_nome_completo"], attr["pe_nome_citacao"], attr["pe_nacionalidade"], attr["pe_contato"], attr["pe_departamento"]])
        conn.commit()
        cur.execute("SELECT pe_id_lattes FROM pessoa WHERE pe_id_lattes = %s", [attr["pe_id_lattes"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in premios_titulos table
def select_id_premios_titulos(attr, conn = None, cur = None):
    """
    Selects in premios_titulos.

    The select_id_premios_titulos function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the premios_titulos table
    :type ev: dict of (<class 'bigint'>, <class 'text'>, <class 'int'>, <class 'text'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT pre_id FROM premios_titulos WHERE pre_nome = %s and pre_ano = %s", [attr["pre_nome"], attr["pre_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in premios_titulos table
def insert_premios_titulos(attr, conn = None, cur = None):
    """
    Inserts in premios_titulos.

    The insert_premios_titulos function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the premios_titulos table
    :type ev: dict of (<class 'bigint'>, <class 'text'>, <class 'int'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO premios_titulos (pre_pe_id, pre_nome, pre_ano, pre_entidade) VALUES (%s, %s, %s, %s)', [attr["pre_pe_id"], attr["pre_nome"], attr["pre_ano"], attr["pre_entidade"]])
        conn.commit()
        cur.execute("SELECT pre_id FROM premios_titulos WHERE pre_nome = %s and pre_ano = %s", [attr["pre_nome"], attr["pre_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in orientacao table
def select_id_orientacao(attr, conn = None, cur = None):
    """
    Selects in orientacao.

    The select_id_orientacao function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the orientacao table
    :type ev: dict of (<class 'bigint'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT ori_id FROM orientacao WHERE ori_pessoa_id = %s and ori_aluno = %s and ori_titulo = %s and ori_ano = %s", [attr["ori_pessoa_id"], attr["ori_aluno"], attr["ori_titulo"], attr["ori_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in orientacao table
def insert_orientacao(attr, conn = None, cur = None):
    """
    Inserts in orientacao.

    The insert_orientacao function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the orientacao table
    :type ev: dict of (<class 'bigint'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO orientacao (ori_pessoa_id, ori_aluno, ori_ano, ori_titulo, ori_pais, ori_instituicao, ori_agencia_fomento, ori_tipo_orientacao, ori_tipo_pesquisa) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)', [attr["ori_pessoa_id"], attr["ori_aluno"], attr["ori_ano"], attr["ori_titulo"], attr["ori_pais"], attr["ori_instituicao"], attr["ori_agencia_fomento"], attr["ori_tipo_orientacao"], attr["ori_tipo_pesquisa"]])
        conn.commit()
        cur.execute("SELECT ori_id FROM orientacao WHERE ori_pessoa_id = %s and ori_aluno = %s and ori_titulo = %s and ori_ano = %s", [attr["ori_pessoa_id"], attr["ori_aluno"], attr["ori_titulo"], attr["ori_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in bancas table
def select_id_bancas(attr, conn = None, cur = None):
    """
    Selects in bancas.

    The select_id_bancas function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the bancas table
    :type ev: dict of (<class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT ban_id FROM bancas WHERE ban_aluno = %s and ban_ano = %s and ban_titulo = %s", [attr["ban_aluno"], attr["ban_ano"], attr["ban_titulo"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in bancas table
def insert_bancas(attr, conn = None, cur = None):
    """
    Inserts in bancas.

    The insert_bancas function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the bancas table
    :type ev: dict of (<class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO bancas (ban_aluno, ban_ano, ban_titulo, ban_pais, ban_instituicao, ban_tipo_pesquisa) VALUES (%s, %s, %s, %s, %s, %s)', [attr["ban_aluno"], attr["ban_ano"], attr["ban_titulo"], attr["ban_pais"], attr["ban_instituicao"], attr["ban_tipo_pesquisa"]])
        conn.commit()
        cur.execute("SELECT ban_id FROM bancas WHERE ban_aluno = %s and ban_ano = %s and ban_titulo = %s", [attr["ban_aluno"], attr["ban_ano"], attr["ban_titulo"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in rel_pessoa_bancas table
def select_id_rel_pessoa_bancas(attr, conn = None, cur = None):
    """
    Selects in rel_pessoa_bancas.

    The select_id_rel_pessoa_bancas function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_bancas table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT pe_ban_id FROM rel_pessoa_bancas WHERE pe_ban_pessoa_id = %s and pe_ban_bancas_id = %s", [attr["pe_ban_pessoa_id"], attr["pe_ban_bancas_id"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in rel_pessoa_bancas table
def insert_rel_pessoa_bancas(attr, conn = None, cur = None):
    """
    Inserts in rel_pessoa_bancas.

    The insert_rel_pessoa_bancas function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_bancas table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO rel_pessoa_bancas (pe_ban_pessoa_id, pe_ban_bancas_id) VALUES (%s, %s)', [attr["pe_ban_pessoa_id"], attr["pe_ban_bancas_id"]])
        conn.commit()
        cur.execute("SELECT pe_ban_id FROM rel_pessoa_bancas WHERE pe_ban_pessoa_id = %s and pe_ban_bancas_id = %s", [attr["pe_ban_pessoa_id"], attr["pe_ban_bancas_id"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in producao_tecnica table
def select_id_producao_tecnica(attr, conn = None, cur = None):
    """
    Selects in producao_tecnica.

    The select_id_producao_tecnica function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the producao_tecnica table
    :type ev: dict of (<class 'text'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT tec_id FROM producao_tecnica WHERE tec_tipo = %s and tec_titulo = %s and tec_ano = %s", [attr["tec_tipo"], attr["tec_titulo"], attr["tec_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in producao_tecnica table
def insert_producao_tecnica(attr, conn = None, cur = None):
    """
    Inserts in producao_tecnica.

    The insert_producao_tecnica function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the producao_tecnica table
    :type ev: dict of (<class 'text'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO producao_tecnica (tec_tipo, tec_titulo, tec_ano, tec_autores, tec_natureza, tec_pais) VALUES (%s, %s, %s, %s, %s, %s)', [attr["tec_tipo"], attr["tec_titulo"], attr["tec_ano"], attr["tec_autores"], attr["tec_natureza"], attr["tec_pais"]])
        conn.commit()
        cur.execute("SELECT tec_id FROM producao_tecnica WHERE tec_tipo = %s and tec_titulo = %s and tec_ano = %s", [attr["tec_tipo"], attr["tec_titulo"], attr["tec_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in rel_pessoa_prod_tec table
def select_id_rel_pessoa_prod_tec(attr, conn = None, cur = None):
    """
    Selects in rel_pessoa_prod_tec.

    The select_id_rel_pessoa_prod_tec function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_prod_tec table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT pe_tec_id FROM rel_pessoa_prod_tec WHERE pe_tec_pessoa_id = %s and pe_tec_prod_tec_id = %s", [attr["pe_tec_pessoa_id"], attr["pe_tec_prod_tec_id"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in rel_pessoa_prod_tec table
def insert_rel_pessoa_prod_tec(attr, conn = None, cur = None):
    """
    Inserts in rel_pessoa_prod_tec.

    The insert_rel_pessoa_prod_tec function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_prod_tec table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO rel_pessoa_prod_tec (pe_tec_pessoa_id, pe_tec_prod_tec_id) VALUES (%s, %s)', [attr["pe_tec_pessoa_id"], attr["pe_tec_prod_tec_id"]])
        conn.commit()
        cur.execute("SELECT pe_tec_id FROM rel_pessoa_prod_tec WHERE pe_tec_pessoa_id = %s and pe_tec_prod_tec_id = %s", [attr["pe_tec_pessoa_id"], attr["pe_tec_prod_tec_id"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in producao_artistica table
def select_id_producao_artistica(attr, conn = None, cur = None):
    """
    Selects in producao_artistica.

    The select_id_producao_artistica function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the producao_artistica table
    :type ev: dict of (<class 'text'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT art_id FROM producao_artistica WHERE art_tipo = %s and art_titulo = %s and art_ano = %s", [attr["art_tipo"], attr["art_titulo"], attr["art_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in producao_artistica table
def insert_producao_artistica(attr, conn = None, cur = None):
    """
    Inserts in producao_artistica.

    The insert_producao_artistica function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the producao_artistica table
    :type ev: dict of (<class 'text'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO producao_artistica (art_tipo, art_titulo, art_ano, art_autores, art_natureza, art_pais) VALUES (%s, %s, %s, %s, %s, %s)', [attr["art_tipo"], attr["art_titulo"], attr["art_ano"], attr["art_autores"], attr["art_natureza"], attr["art_pais"]])
        conn.commit()
        cur.execute("SELECT art_id FROM producao_artistica WHERE art_tipo = %s and art_titulo = %s and art_ano = %s", [attr["art_tipo"], attr["art_titulo"], attr["art_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in rel_pessoa_prod_art table
def select_id_rel_pessoa_prod_art(attr, conn = None, cur = None):
    """
    Selects in rel_pessoa_prod_art.

    The select_id_rel_pessoa_prod_art function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_prod_art table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT pe_art_id FROM rel_pessoa_prod_art WHERE pe_art_pessoa_id = %s and pe_art_prod_art_id = %s", [attr["pe_art_pessoa_id"], attr["pe_art_prod_art_id"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in rel_pessoa_prod_art table
def insert_rel_pessoa_prod_art(attr, conn = None, cur = None):
    """
    Inserts in rel_pessoa_prod_art.

    The insert_rel_pessoa_prod_art function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_prod_art table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO rel_pessoa_prod_art (pe_art_pessoa_id, pe_art_prod_art_id) VALUES (%s, %s)', [attr["pe_art_pessoa_id"], attr["pe_art_prod_art_id"]])
        conn.commit()
        cur.execute("SELECT pe_art_id FROM rel_pessoa_prod_art WHERE pe_art_pessoa_id = %s and pe_art_prod_art_id = %s", [attr["pe_art_pessoa_id"], attr["pe_art_prod_art_id"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in producao_bibliografica table
def select_id_producao_bibliografica(attr, conn = None, cur = None):
    """
    Selects in producao_bibliografica.

    The select_id_producao_bibliografica function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the producao_bibliografica table
    :type ev: dict of (<class 'text'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT bib_id FROM producao_bibliografica WHERE bib_tipo = %s and bib_titulo = %s and bib_ano = %s", [attr["bib_tipo"], attr["bib_titulo"], attr["bib_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in producao_bibliografica table
def insert_producao_bibliografica(attr, conn = None, cur = None):
    """
    Inserts in producao_bibliografica.

    The insert_producao_bibliografica function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the producao_bibliografica table
    :type ev: dict of (<class 'text'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO producao_bibliografica (bib_tipo, bib_titulo, bib_ano, bib_autores, bib_publicacao, bib_editora, bib_edicao, bib_volume, bib_natureza, bib_pais) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', [attr["bib_tipo"], attr["bib_titulo"], attr["bib_ano"], attr["bib_autores"], attr["bib_publicacao"], attr["bib_editora"], attr["bib_edicao"], attr["bib_volume"], attr["bib_natureza"], attr["bib_pais"]])
        conn.commit()
        cur.execute("SELECT bib_id FROM producao_bibliografica WHERE bib_tipo = %s and bib_titulo = %s and bib_ano = %s", [attr["bib_tipo"], attr["bib_titulo"], attr["bib_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in rel_pessoa_prod_bib table
def select_id_rel_pessoa_prod_bib(attr, conn = None, cur = None):
    """
    Selects in rel_pessoa_prod_bib.

    The select_id_rel_pessoa_prod_bib function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_prod_bib table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT pe_bib_id FROM rel_pessoa_prod_bib WHERE pe_bib_pessoa_id = %s and pe_bib_prod_bib_id = %s", [attr["pe_bib_pessoa_id"], attr["pe_bib_prod_bib_id"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in rel_pessoa_prod_bib table
def insert_rel_pessoa_prod_bib(attr, conn = None, cur = None):
    """
    Inserts in rel_pessoa_prod_bib.

    The insert_rel_pessoa_prod_bib function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_prod_bib table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO rel_pessoa_prod_bib (pe_bib_pessoa_id, pe_bib_prod_bib_id) VALUES (%s, %s)', [attr["pe_bib_pessoa_id"], attr["pe_bib_prod_bib_id"]])
        conn.commit()
        cur.execute("SELECT pe_bib_id FROM rel_pessoa_prod_bib WHERE pe_bib_pessoa_id = %s and pe_bib_prod_bib_id = %s", [attr["pe_bib_pessoa_id"], attr["pe_bib_prod_bib_id"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in participacao table
def select_id_participacao(attr, conn = None, cur = None):
    """
    Selects in participacao.

    The select_id_participacao function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the participacao table
    :type ev: dict of (<class 'text'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT par_id FROM participacao WHERE par_tipo = %s and par_nome_ev = %s and par_ano = %s", [attr["par_tipo"], attr["par_nome_ev"], attr["par_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in participacao table
def insert_participacao(attr, conn = None, cur = None):
    """
    Inserts in participacao.

    The insert_participacao function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the participacao table
    :type ev: dict of (<class 'text'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO participacao (par_nome_ev, par_tipo, par_ano, par_natureza, par_pais) VALUES (%s, %s, %s, %s, %s)', [attr["par_nome_ev"], attr["par_tipo"], attr["par_ano"], attr["par_natureza"], attr["par_pais"]])
        conn.commit()
        cur.execute("SELECT par_id FROM participacao WHERE par_tipo = %s and par_nome_ev = %s and par_ano = %s", [attr["par_tipo"], attr["par_nome_ev"], attr["par_ano"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in rel_pessoa_participacao table
def select_id_rel_pessoa_participacao(attr, conn = None, cur = None):
    """
    Selects in rel_pessoa_participacao.

    The select_id_rel_pessoa_participacao function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_participacao table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT pe_par_id FROM rel_pessoa_participacao WHERE pe_par_pessoa_id = %s and pe_par_participacao_id = %s and pe_par_titulo = %s", [attr["pe_par_pessoa_id"], attr["pe_par_participacao_id"], attr["pe_par_titulo"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in rel_pessoa_participacao table
def insert_rel_pessoa_participacao(attr, conn = None, cur = None):
    """
    Inserts in rel_pessoa_participacao.

    The insert_rel_pessoa_participacao function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_participacao table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO rel_pessoa_participacao (pe_par_pessoa_id, pe_par_participacao_id, pe_par_titulo, pe_par_forma_par, pe_par_tipo_par) VALUES (%s, %s, %s, %s, %s)', [attr["pe_par_pessoa_id"], attr["pe_par_participacao_id"], attr["pe_par_titulo"], attr["pe_par_forma_par"], attr["pe_par_tipo_par"]])
        conn.commit()
        cur.execute("SELECT pe_par_id FROM rel_pessoa_participacao WHERE pe_par_pessoa_id = %s and pe_par_participacao_id = %s and pe_par_titulo = %s", [attr["pe_par_pessoa_id"], attr["pe_par_participacao_id"], attr["pe_par_titulo"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an select in colaboradores table
def select_id_colaboradores(attr, conn = None, cur = None):
    """
    Selects in colaboradores.

    The select_id_colaboradores function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the colaboradores table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT co_id FROM colaboradores WHERE co_pe_id1 = %s and co_pe_id2 = %s", [attr["co_pe_id1"], attr["co_pe_id2"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()

# Makes an insert in colaboradores table
def insert_colaboradores(attr, conn = None, cur = None):
    """
    Inserts in colaboradores.

    The insert_colaboradores function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the colaboradores table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO colaboradores (co_pe_id1, co_pe_id2) VALUES (%s, %s)', [attr["co_pe_id1"], attr["co_pe_id2"]])
        conn.commit()
        cur.execute("SELECT co_id FROM colaboradores WHERE co_pe_id1 = %s and co_pe_id2 = %s", [attr["co_pe_id1"], attr["co_pe_id2"]])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()
