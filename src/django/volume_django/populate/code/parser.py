import os
import sys
import pdb
from lxml import etree
import xml.etree.ElementTree as ET

from Parser.orientacao import *
from Parser.premios_titulos import *
from Parser.bancas import *
from Parser.pessoa import *
from Parser.producao_bibliografica import *
from Parser.producao_tecnica import *
from Parser.producao_artistica import *
from Parser.participacao import *

from DB.connection import *
from DB.general_operations import *
from DB.select_operations import *

def insert_all_colaboradores(num_identificador, autores, conn = None, cur = None):
    for autor in autores:
        id = select_id_pessoa_by_name(autor, conn, cur)
        if not id is None and int(num_identificador) != id:
            dict = {'co_pe_id1': num_identificador, 'co_pe_id2': id}
            id = select_id_colaboradores(dict, conn, cur)
            if id is None:
                id = insert_colaboradores(dict, conn, cur)

def parse_insert_pessoa(file, department, conn = None, cur = None):
    parser = etree.XMLParser(recover=True)
    tree = ET.parse(file)
    root = tree.getroot()
    num_identificador = file[11:-4]

    print("DEPARTAMENTO")
    print(department)
    pessoa = parse_pessoa(root, department, num_identificador)

    id = select_id_pessoa(pessoa, conn, cur)
    if id is None:
        id = insert_pessoa(pessoa, conn, cur)

def parse_insert_others(file, department, conn = None, cur = None):
    parser = etree.XMLParser(recover=True)
    tree = ET.parse(file)
    root = tree.getroot()
    num_identificador = file[11:-4]

    premios_titulos = parse_premios_titulos(root, num_identificador)
    for premio_titulo in premios_titulos:
        id = select_id_premios_titulos(premio_titulo, conn, cur)
        if id is None:
            id = insert_premios_titulos(premio_titulo, conn, cur)

    orientacoes = parse_orientacao(root, num_identificador)
    for orientacao in orientacoes:
        id = select_id_orientacao(orientacao, conn, cur)
        if id is None:
            id = insert_orientacao(orientacao, conn, cur)

    bancas = parse_bancas(root, num_identificador)
    for banca in bancas:
        id = select_id_bancas(banca, conn, cur)
        if id is None:
            id = insert_bancas(banca, conn, cur)
        dict = {'pe_ban_pessoa_id': num_identificador, 'pe_ban_bancas_id': id}

        id = select_id_rel_pessoa_bancas(dict, conn, cur)
        if id is None:
            id = insert_rel_pessoa_bancas(dict, conn, cur)

    producoes_tecnicas = parse_producao_tecnica(root, num_identificador)
    for producao in producoes_tecnicas:
        id = select_id_producao_tecnica(producao, conn, cur)
        if id is None:
            id = insert_producao_tecnica(producao, conn, cur)
        dict = {'pe_tec_pessoa_id': num_identificador, 'pe_tec_prod_tec_id': id}

        id = select_id_rel_pessoa_prod_tec(dict, conn, cur)
        if id is None:
            id = insert_rel_pessoa_prod_tec(dict, conn, cur)

        insert_all_colaboradores(num_identificador, producao['tec_autores'])

    producoes_artisticas = parse_producao_artistica(root, num_identificador)
    for producao in producoes_artisticas:
        id = select_id_producao_artistica(producao, conn, cur)
        if id is None:
            id = insert_producao_artistica(producao, conn, cur)
        dict = {'pe_art_pessoa_id': num_identificador, 'pe_art_prod_art_id': id}

        id = select_id_rel_pessoa_prod_art(dict, conn, cur)
        if id is None:
            id = insert_rel_pessoa_prod_art(dict, conn, cur)

        insert_all_colaboradores(num_identificador, producao['art_autores'])

    producoes_bibliograficas = parse_producao_bibliografica(root, num_identificador)
    for producao in producoes_bibliograficas:
        id = select_id_producao_bibliografica(producao, conn, cur)
        if id is None:
            id = insert_producao_bibliografica(producao, conn, cur)
        dict = {'pe_bib_pessoa_id': num_identificador, 'pe_bib_prod_bib_id': id}

        id = select_id_rel_pessoa_prod_bib(dict, conn, cur)
        if id is None:
            id = insert_rel_pessoa_prod_bib(dict, conn, cur)

        insert_all_colaboradores(num_identificador, producao['bib_autores'])

    participacoes = parse_participacao(root, num_identificador)
    for participacao in participacoes:
        id = select_id_participacao(participacao, conn, cur)
        if id is None:
            id = insert_participacao(participacao, conn, cur)
        dict = {'pe_par_pessoa_id': num_identificador,
                'pe_par_participacao_id': id,
                'pe_par_titulo': participacao['par_titulo'],
                'pe_par_forma_par': participacao['par_forma_par'],
                'pe_par_tipo_par': participacao['par_tipo_par'],
                }

        id = select_id_rel_pessoa_participacao(dict, conn, cur)
        if id is None:
            id = insert_rel_pessoa_participacao(dict, conn, cur)


def main():
    #pdb.set_trace()
    zip_path = '../xml/'
    department = ["AUH", "AUP", "AUT"]

    conn, cur = get_db()

    verb = False
    if len(sys.argv) > 1 and sys.argv[1] == "-v":
        verb = True

    # Inserts in pessoa
    for dep in department:
        path = zip_path + dep + "/"
        for r, d, f in os.walk(path):
            for file in f:
                if verb:
                    print("\n------ inserting {} in pessoa ------\n".format(os.path.join(r, file)[11:-4]))
                parse_insert_pessoa(os.path.join(r, file), dep, conn, cur)

    # Inserts in other tables
    for dep in department:
        path = zip_path + dep + "/"
        for r, d, f in os.walk(path):
            for file in f:
                if verb:
                    print("\n------ inserting data from {} ------\n".format(os.path.join(r, file)[11:-4]))
                parse_insert_others(os.path.join(r, file), dep, conn, cur)

if __name__ == "__main__":
    main()
