import json
from django.test import TestCase, Client
from django.urls import reverse

from lattes_data.models import ProducaoBibliografica, RelPessoaProdBib, Pessoa


class ViewProducaoBibliograficaTests(TestCase):
    """ Test module for producao_bibliografica views """
    # fixture file
    fixtures = [
                'util/fixtures/docentes.json', 
                'producao_bibliografica/fixtures/tests.json'
            ]

    # initialize the APIClient app
    client = Client()

    def test_index_default_year_range_returns_all_elements(self):
        # get API response
        response = self.client.get(
            reverse('producao_bibliografica-index'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        producao_list = json_response['result']

        # returns 5 elements from 2010 to 2019
        self.assertEqual(len(producao_list), 5)

    def test_index_with_valid_year_range_returns_all_elements(self):
        # get API response
        response = self.client.get(
            reverse('producao_bibliografica-index'),
            {'ano_inicio': 2015, 'ano_fim': 2018})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        producao_list = json_response['result']

        # returns 3 elements from 2015 to 2018
        self.assertEqual(len(producao_list), 3)

    def test_index_with_small_page_size(self):
        # get API response
        response = self.client.get(
            reverse('producao_bibliografica-index'), {'page_size': 2})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        producao_list = json_response['result']

        self.assertEqual(len(producao_list), 2)

    def test_index_pagination(self):
        # get API response
        response = self.client.get(
            reverse('producao_bibliografica-index'), {'page_size': 2, 'page': 1})
        response_first_page = self.client.get(
            reverse('producao_bibliografica-index'), {'page_size': 1, 'page': 1})
        response_second_page = self.client.get(
            reverse('producao_bibliografica-index'), {'page_size': 1, 'page': 2})
        json_response = json.loads(response.content)
        json_response_first_page = json.loads(response_first_page.content)
        json_response_second_page = json.loads(response_second_page.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'code' in json_response_first_page
        self.assertEqual(json_response_first_page['code'], 200)

        assert 'code' in json_response_second_page
        self.assertEqual(json_response_second_page['code'], 200)

        assert 'result' in json_response
        producao_list = json_response['result']

        assert 'result' in json_response_first_page
        producao_list = json_response_first_page['result']

        assert 'result' in json_response_second_page
        producao_list = json_response_second_page['result']

        first_element = json_response['result'][0]
        second_element = json_response['result'][1]
        self.assertEqual(json_response_first_page['result'][0], first_element)
        self.assertEqual(
            json_response_second_page['result'][0], second_element)

    def test_index_for_specific_year_returns_correct_element(self):
        # get API response
        response = self.client.get(
            reverse('producao_bibliografica-index'),
            {'ano_inicio': 1998, 'ano_fim': 1998})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        producao_list = json_response['result']

        expected_object = ProducaoBibliografica.objects.filter(bib_ano=1998)[
            :1].get()

        self.assertEqual(len(producao_list), 1)

        object = producao_list[0]
        assert 'titulo' in object
        self.assertEqual(object['titulo'], expected_object.bib_titulo)

        assert 'ano' in object
        self.assertEqual(object['ano'], expected_object.bib_ano)

        assert 'tipo' in object
        self.assertEqual(object['tipo'], expected_object.bib_tipo)

    def test_index_with_empty_database(self):
        RelPessoaProdBib.objects.all().delete()
        ProducaoBibliografica.objects.all().delete()

        # get API response
        response = self.client.get(reverse('producao_bibliografica-index'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        producao_list = json_response['result']
        self.assertEqual(len(producao_list), 0)

    def test_index_invalid_year_range_returns_404(self):
        # get API response
        response = self.client.get(
            reverse('producao_bibliografica-index'),
            {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_index_invalid_year_returns_404(self):
        # get API response
        response = self.client.get(
            reverse('producao_bibliografica-index'),
            {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_returns_all_departments(self):
        response = self.client.get(reverse('producao_bibliografica-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        expected_departments = [dep['pe_departamento']
                                for dep in Pessoa.objects.values('pe_departamento').distinct()]

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(prod_list), len(expected_departments))

        for dep in prod_list:
            assert dep in expected_departments

    def test_count_returns_all_types(self):
        response = self.client.get(reverse('producao_bibliografica-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        expected_types = [typ['bib_tipo']
                          for typ in ProducaoBibliografica.objects.values('bib_tipo').distinct()]

        for dep in prod_list:
            self.assertEqual(len(prod_list[dep]), len(expected_types))
            for type in prod_list[dep]:
                assert type in expected_types

                # 10 keys: one for each year
                self.assertEqual(len(prod_list[dep][type]), 10)

    def test_count_with_specific_department(self):
        response = self.client.get(
            reverse('producao_bibliografica-count'), {'departamentos[]': 'AUH'})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)

        self.assertEqual(len(prod_list), 1)
        self.assertEqual(list(prod_list.keys()), ['AUH'])

    def test_count_with_specific_type(self):
        types = ['tipo1', 'teste']
        response = self.client.get(
            reverse('producao_bibliografica-count'), {'tipos[]': types})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)

        for dep in prod_list:
            type_list = list(prod_list[dep].keys())
            self.assertEqual(len(type_list), len(types))
            self.assertEqual(sorted(type_list), sorted(types))

    def test_count_returns_correctly_for_each_year(self):
        response = self.client.get(reverse('producao_bibliografica-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(prod_list['AUH']['tipo1']['2011'], 1)
        self.assertEqual(prod_list['AUT']['tipo2']['2015'], 1)
        self.assertEqual(prod_list['AUH']['tipo1']['2015'], 1)
        self.assertEqual(prod_list['AUT']['tipo2']['2019'], 1)
        self.assertEqual(prod_list['AUH']['tipo2']['2019'], 1)
        self.assertEqual(prod_list['AUP']['tipo2']['2019'], 1)

    def test_count_returns_correctly_for_specific_year_range(self):
        response = self.client.get(
            reverse('producao_bibliografica-count'), {'ano_inicio': 2017, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)

        for dep in prod_list:
            for type in prod_list[dep]:
                self.assertEqual(len(prod_list[dep][type]), 3)

    def test_count_returns_404_with_invalid_range(self):
        response = self.client.get(
            reverse('producao_bibliografica-count'), {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_returns_404_with_invalid_year(self):
        response = self.client.get(
            reverse('producao_bibliografica-count'), {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_with_empty_database(self):
        RelPessoaProdBib.objects.all().delete()
        ProducaoBibliografica.objects.all().delete()

        response = self.client.get(reverse('producao_bibliografica-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        for dep in prod_list:
            for type in prod_list[dep]:
                for year in prod_list[dep][type]:
                    self.assertEqual(prod_list[dep][type], 0)

    def test_keywords_with_default_limit(self):
        response = self.client.get(reverse('producao_bibliografica-keywords'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        expected_result = [
            {"titulo": 6},
            {"teste": 3},
            {"foo": 2},
            {"bar": 1}
        ]
        self.assertEqual(json_response['result'], expected_result)

    def test_keywords_with_specific_limit(self):
        response = self.client.get(
            reverse('producao_bibliografica-keywords'), {"limit": 2})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        expected_result = [
            {"titulo": 6},
            {"teste": 3}
        ]
        self.assertEqual(json_response['result'], expected_result)

    def test_keywords_with_empty_database(self):
        RelPessoaProdBib.objects.all().delete()
        ProducaoBibliografica.objects.all().delete()

        response = self.client.get(reverse('producao_bibliografica-keywords'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        self.assertEqual(json_response['result'], [])

    def test_keywords_with_negative_limit_returns_404(self):
        response = self.client.get(
            reverse('producao_bibliografica-keywords'), {"limit": -1})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_keywords_with_invalid_limit_returns_404(self):
        response = self.client.get(
            reverse('producao_bibliografica-keywords'), {"limit": "foo"})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_map_with_default_limit(self):
        response = self.client.get(reverse('producao_bibliografica-map'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        expected_result = [
            {'Brasil': {'lat': '-33.8689056', 'long': '5.2693306', 'total': 3}}, 
            {'Espanha': {'lat': '27.4335426', 'long': '43.9933088', 'total': 1}}, 
            {'Cazaquistão': {'lat': '40.5686476', 'long': '55.4421701', 'total': 1}}, 
            {'Austrália': {'lat': '-55.3228175', 'long': '-9.0880125', 'total': 1}}
        ]
        self.assertEqual(json_response['result'], expected_result)

    def test_map_with_specific_limit(self):
        response = self.client.get(
            reverse('producao_bibliografica-map'), {"limit": 2})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        expected_result = [
            {'Brasil': {'lat': '-33.8689056', 'long': '5.2693306', 'total': 3}}, 
            {'Espanha': {'lat': '27.4335426', 'long': '43.9933088', 'total': 1}}
        ]
        self.assertEqual(json_response['result'], expected_result)

    def test_map_with_empty_database(self):
        RelPessoaProdBib.objects.all().delete()
        ProducaoBibliografica.objects.all().delete()

        response = self.client.get(reverse('producao_bibliografica-map'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        self.assertEqual(json_response['result'], [])

    def test_map_with_negative_limit_returns_404(self):
        response = self.client.get(
            reverse('producao_bibliografica-map'), {"limit": -1})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_map_with_invalid_limit_returns_404(self):
        response = self.client.get(
            reverse('producao_bibliografica-map'), {"limit": "foo"})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_tipos_returns_correctly(self):
        response = self.client.get(reverse('producao_bibliografica-tipos'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        expected_types = [typ['bib_tipo']
                          for typ in ProducaoBibliografica.objects.values('bib_tipo').distinct()]
        self.assertEqual(len(json_response['result']), len(expected_types))
        self.assertEqual(
            sorted(json_response['result']), sorted(expected_types))

    def test_count_tipos_returns_all_departments(self):
        response = self.client.get(
            reverse('producao_bibliografica-count_tipos'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        prod_list = json_response['result']

        expected_departments = [dep['pe_departamento']
                                for dep in Pessoa.objects.values('pe_departamento').distinct()]
        returned_departments = list(prod_list.keys())

        self.assertEqual(len(returned_departments), len(expected_departments))
        self.assertEqual(sorted(returned_departments),
                         sorted(expected_departments))

    def test_count_tipos_returns_all_types(self):
        response = self.client.get(
            reverse('producao_bibliografica-count_tipos'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        prod_list = json_response['result']

        expected_types = [typ['bib_tipo']
                          for typ in ProducaoBibliografica.objects.values('bib_tipo').distinct()]

        for dep in prod_list:
            self.assertEqual(len(prod_list[dep]), len(expected_types))
            returned_types = []
            for value in prod_list[dep]:
                returned_types.extend(list(value.keys()))
            self.assertEqual(sorted(returned_types), sorted(expected_types))

    def test_count_tipos_with_specific_department(self):
        response = self.client.get(
            reverse('producao_bibliografica-count_tipos'), {'departamentos[]': 'AUH'})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)

        self.assertEqual(len(prod_list), 1)
        self.assertEqual(list(prod_list.keys()), ['AUH'])

    def test_count_tipos_with_specific_type(self):
        types = ['tipo1', 'teste']
        response = self.client.get(
            reverse('producao_bibliografica-count_tipos'), {'tipos[]': types})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)

        for dep in prod_list:
            returned_types = []
            for value in prod_list[dep]:
                returned_types.extend(list(value.keys()))
            self.assertEqual(len(returned_types), len(types))
            self.assertEqual(sorted(returned_types), sorted(types))

    def test_count_tipos_returns_correctly(self):
        response = self.client.get(
            reverse('producao_bibliografica-count_tipos'), {
                'departamentos[]': ['AUH'],
                'tipos[]': ['tipo1']
            })
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        prod_list = json_response['result']

        self.assertEqual(len(prod_list['AUH']), 1)
        self.assertEqual(prod_list['AUH'][0]['tipo1'], 4)

    def test_rank_returns_all_docentes_with_default_departments_and_types(self):
        response = self.client.get(reverse('producao_bibliografica-rank'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        ranking_list = json_response['result']

        expected_docentes = Pessoa.objects.all()

        self.assertEqual(len(ranking_list), len(expected_docentes))

    def test_rank_with_specific_department(self):
        response = self.client.get(
            reverse('producao_bibliografica-rank'), {'departamentos[]': 'AUH'})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        ranking_list = json_response['result']

        for docente in ranking_list:
            docente_in_db = Pessoa.objects.filter(
                pe_nome_completo=docente['nome'])[0]
            self.assertEqual(docente_in_db.pe_departamento, 'AUH')

    def test_rank_with_specific_type(self):
        types = ['tipo1', 'teste']
        response = self.client.get(
            reverse('producao_bibliografica-rank'), {'tipos[]': types})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        ranking_list = json_response['result']

        for docente in ranking_list:
            docente_in_db = Pessoa.objects.filter(
                pe_nome_completo=docente['nome'])
            productions = ProducaoBibliografica.objects.filter(bib_ano__gte=2010,
                                                               bib_ano__lte=2019,
                                                               bib_tipo__in=types,
                                                               autores__in=docente_in_db)\
                .count()

            self.assertEqual(productions, docente['numero_de_producoes'])

    def test_rank_returns_correctly_for_specific_year_range(self):
        response = self.client.get(
            reverse('producao_bibliografica-rank'), {
                'departamentos[]': ['AUH'],
                'tipos[]': ['tipo1'],
                'ano_inicio': 1998,
                'ano_fim': 1998
            })
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        ranking_list = json_response['result']

        expected_result = [{
            "nome": "John Lennon",
            "numero_de_producoes": 1
        }]
        self.assertEqual(ranking_list, expected_result)

    def test_rank_with_specific_limit(self):
        response = self.client.get(
            reverse('producao_bibliografica-rank'), {"limit": 3})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        expected_result = [
            {
                "nome": "Zachery Gonzalez",
                "numero_de_producoes": 2
            },
            {
                "nome": "Izabelle Wormald",
                "numero_de_producoes": 2
            },
            {
                "nome": "Lucas Bate",
                "numero_de_producoes": 2
            }
        ]
        self.assertEqual(json_response['result'], expected_result)

    def test_rank_returns_404_with_invalid_range(self):
        response = self.client.get(
            reverse('producao_bibliografica-rank'), {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_rank_returns_404_with_invalid_year(self):
        response = self.client.get(
            reverse('producao_bibliografica-rank'), {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_rank_returns_404_with_invalid_limit(self):
        response = self.client.get(
            reverse('producao_bibliografica-rank'), {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)
