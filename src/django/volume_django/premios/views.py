from django.core.paginator import Paginator
from django.db.models import Count
from django.http import JsonResponse
from util.format import make_chart_js
from lattes_data.models import PremiosTitulos, Pessoa
import datetime

now = datetime.datetime.now()
actual_year = str(now.year)

def error(msg):
    response_data = {
        'code': 404,
        'result': msg,
    }
    return JsonResponse(response_data)


def index(request):
    """Index for premios route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim' and 
    'format' parameters.

    Returns:
    JsonResponse with status code and a list of premios for each year.
   """

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    page = request.GET.get('page', '1')
    page_size = request.GET.get('page_size', '15')
 
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    if not page.isdigit() or not page_size.isdigit():
        return error('Página ou tamanho de página inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    result = []
    query_set = PremiosTitulos.objects.filter(
        pre_ano__gte=int(ini_year), pre_ano__lte=int(end_year)).order_by('-pre_ano')
    premio_page = Paginator(query_set, page_size)

    for premio in premio_page.page(page):
        teachers = []
        teachers.append({
            "nome": premio.pre_pe.pe_nome_completo,
            "id_attes": premio.pre_pe.pe_id_lattes,
            "departamento": premio.pre_pe.pe_departamento
        })
        result.append({
            "ano": premio.pre_ano,
            "nome_premio": premio.pre_nome,
            "entidade_premio": premio.pre_entidade,
            "docente": teachers
        })

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)

def count(request):
    """Count for premios route.

    Parameters: a GET request with 'ano_inicio', 'ano_fim' and 
    'departamentos[]' parameters.

    Returns:
    JsonResponse with status code and the count of premios for each year
    for each departamento.
   """

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', '2019')
    departments = request.GET.getlist('departamentos[]', '')

    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    
    if not departments:
        departments = [dep['pe_departamento']
        for dep in Pessoa.objects.values('pe_departamento').distinct()]

    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for dep in departments:
        result[dep] = {}
        pessoa = Pessoa.objects.filter(pe_departamento__contains=dep).values('pe_id_lattes')
        premios = PremiosTitulos.objects.filter(pre_pe__in=pessoa).distinct()
        for l in labels:
            count = premios.filter(pre_ano=l).aggregate(Count('pre_id'))
            result[dep][l] = count['pre_id__count']

    if format == 'chartjs':
        result = make_chart_js(result)

    response_data = {
        "code": 200, 
        "result": result
    }

    return JsonResponse(response_data)



def rank(request):
    """Rank for premios route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim', 
    'limit' and 'departamentos[]' parameters.

    Returns:
    JsonResponse with status code and a list of the <limit> teachers
    with the most awards.
   """

    try:
        limit = int(request.GET.get('limit', 10))
    except ValueError:
        return error('Limite inválido.')

    if not limit > 0:
        return error('Limite inválido.')

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    departments = request.GET.getlist('departamentos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]

    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido.')

    ini_year, end_year = int(ini_year), int(end_year)

    result = []

    pessoas = Pessoa.objects.filter(pe_departamento__in=departments)

    for p in pessoas:
        premios = p.premiostitulos_set.filter(pre_ano__lte=int(end_year), pre_ano__gte=int(ini_year))\
        .aggregate(Count('pre_id'))
        result.append((p.pe_nome_completo, p.pe_departamento, premios['pre_id__count']))

    result.sort(key=lambda tup: tup[2], reverse=True)


    response_data = {
        "code": 200, 
        "result": result[:int(limit)]
    }

    return JsonResponse(response_data)