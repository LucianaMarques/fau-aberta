import json
from django.test import TestCase, Client
from django.urls import reverse

from lattes_data.models import Pessoa, PremiosTitulos


class ViewPremiosTests(TestCase):
    """ Test module for premios views """
    # fixture file
    fixtures = ['util/fixtures/docentes.json', 'premios/fixtures/tests.json']

    # initialize the APIClient app
    client = Client()
        
    def test_index(self):
        # get API response
        response = self.client.get(reverse('premios-index'), {'format': 'list'})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(premios_list), 6)

    def test_index_year(self):
        # get API response
        response = self.client.get(reverse('premios-index'), {'format': 'default'})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(premios_list['2015']), 3)  

    def test_index_year_range(self):
        # get API response
        response = self.client.get(reverse('premios-index'), {'ano_inicio': 2017, 'ano_fim': 2019})
        json_response = json.loads(response.content)
        
        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(premios_list), 3)

    def test_index_invalid_year(self):
        # get API response
        response = self.client.get(reverse('premios-index'), {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)
        
    def test_index_invalid_year2(self):
        # get API response
        response = self.client.get(reverse('premios-index'), {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)
    
    def test_index_invalid_format(self):
        # get API response
        response = self.client.get(reverse('premios-index'), {'format': 'zebra'})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)
    
    def test_index_with_empty_database(self):
        PremiosTitulos.objects.all().delete()
        
        # get API response
        response = self.client.get(reverse('premios-index'), {'format': 'list'})
        json_response = json.loads(response.content)
        
        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(premios_list), 0)

    def test_count(self):
        response = self.client.get(reverse('premios-count'))
        json_response = json.loads(response.content)
        
        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(premios_list), 3)

        for item in premios_list:
            assert item in ['AUH', 'AUP', 'AUT']
            self.assertEqual(len(premios_list[item]), 10)

    def test_count_year(self):
        response = self.client.get(reverse('premios-count'))
        json_response = json.loads(response.content)
        

        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(premios_list['AUH']['2014'], 1)
        self.assertEqual(premios_list['AUH']['2015'], 2)
        self.assertEqual(premios_list['AUH']['2017'], 1)
        self.assertEqual(premios_list['AUH']['2018'], 1)
        self.assertEqual(premios_list['AUP']['2015'], 1)
  
    def test_count_year_range(self):
        response = self.client.get(reverse('premios-count'), {'ano_inicio': 2017, 'ano_fim': 2019})
        json_response = json.loads(response.content)  

        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(premios_list), 3)

        for item in premios_list:
            assert item in ['AUH', 'AUP', 'AUT']
            self.assertEqual(len(premios_list[item]), 3)

    def test_count_invalid_year(self):
        response = self.client.get(reverse('premios-count'), {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_invalid_year2(self):
        response = self.client.get(reverse('premios-count'), {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_default_format(self):
        response1 = self.client.get(reverse('premios-count'))
        json_response1 = json.loads(response1.content)
        response2 = self.client.get(reverse('premios-count'), {'format': 'default'})
        json_response2 = json.loads(response2.content)
        self.assertEqual(json_response1, json_response2)

    def test_count_invalid_format(self):
        response = self.client.get(reverse('premios-count'), {'format': 'zebra'})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_validate_chartjs(self):
        response = self.client.get(reverse('premios-count'), {'format': 'chartjs'})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 200)

        result = json_response['result']
        assert 'datasets' in result
        assert 'labels' in result

        datasets = result['datasets']
        for item in datasets:
            assert 'label' in item
            assert 'data' in item
            for i in item:
                if i == 'data':
                    self.assertEqual(len(item[i]), 10)
                elif i == 'label':
                    assert item[i] in ['AUH', 'AUP', 'AUT']

        labels = result['labels']
        self.assertEqual(len(labels), 10)

    def test_count_with_empty_database(self):
        PremiosTitulos.objects.all().delete()

        response = self.client.get(reverse('premios-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(premios_list), 3)

        for item in premios_list:
            assert item in ['AUH', 'AUP', 'AUT']
            years = premios_list[item]
            for value in years:
                self.assertEqual(years[value], 0)

    def test_rank(self):
        response = self.client.get(reverse('premios-rank'))
        json_response = json.loads(response.content)
        
        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(premios_list), 6)


    def test_rank_order(self):
        response = self.client.get(reverse('premios-rank'))
        json_response = json.loads(response.content)
        
        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        prev = None
        for item in premios_list:
            self.assertEqual(len(item), 3)
            if prev is not None:
                assert item[2] <= prev
            prev = item[2]
  
    def test_rank_year_range(self):
        response = self.client.get(reverse('premios-rank'), {'ano_inicio': 2017, 'ano_fim': 2019})
        json_response = json.loads(response.content)  

        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(premios_list), 6)

        for item in premios_list:
            assert item[0] in ['John Lennon', 'Zachery Gonzalez', 'Izabelle Wormald', 'Alessandra Lewis', 'Lucas Bate', 'Saara Barnard']
            if item[0] == 'John Lennon' or item[0] == 'Zachery Gonzalez':
                self.assertEqual(item[2], 1)
            else:
                self.assertEqual(item[2], 0)

    def test_rank_invalid_year(self):
        response = self.client.get(reverse('premios-rank'), {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_rank_invalid_year2(self):
        response = self.client.get(reverse('premios-rank'), {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_with_empty_database(self):
        PremiosTitulos.objects.all().delete()

        response = self.client.get(reverse('premios-rank'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        premios_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(premios_list), 6)

        for item in premios_list:
            self.assertEqual(item[2], 0)
        