from django.db.models import Count
from django.http import JsonResponse
from lattes_data.models import ProducaoArtistica, ProducaoBibliografica, ProducaoTecnica,\
Orientacao, Bancas, PremiosTitulos


def index(request):
    """Index for producoes route.

    Parameters: a GET request no parameters.

    Returns:
    JsonResponse with status code and the count of productions for each table.
   """

    dict = {'Produção Artística': [ProducaoArtistica, 'art_id'], 
    'Produção Técnica': [ProducaoTecnica, 'tec_id'],
    'Produção Bibliográfica': [ProducaoBibliografica, 'bib_id'],
    'Orientação': [Orientacao, 'ori_id'],
    'Bancas': [Bancas, 'ban_id'], 
    'Prêmios e Títulos': [PremiosTitulos, 'pre_id']}

    result = {}
    for key, value in dict.items():
        result[key] = value[0].objects.aggregate(Count(value[1]))[value[1]+'__count']

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)