from django.contrib import admin
from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /producoes/
    url(r'^$', views.index, name='dashboard-index'),
]
