import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { secondary } from '../styles'

function Menu() {
    return (
        <Container>
            <Link to='/producao-bibliografica'>Produção bibliográfica</Link>
            <Link to='/producao-artistica'>Produção artística</Link>
            <Link to='/producao-tecnica'>Produção técnica</Link>
            <Link to='/orientacoes'>Orientações</Link>
            <Link to='/bancas'>Bancas</Link>
            <Link to='/premios'>Prêmios</Link>
        </Container>
    )
}

export default Menu

const Container = styled.div`
    background-color: ${secondary};
    position: fixed;
    top: 136px;
    height: 48px;
    width: 100%;
    display: flex;
    justify-content: space-around;
    align-items: center;
    border-top: 5px solid white;
    z-index: 9999;
    > a {
        color: white;
        font-size: 20px;
        text-decoration: none;
        :hover {
            text-decoration: underline;
        }
    }
    font-family: 'Source Sans Pro', sans-serif;
`