import React from 'react'
import { ComposableMap, Geographies, Geography, Marker } from 'react-simple-maps'
import geoMap from './world-map.json'

const markers = [
    { name: "Brasília", coordinates: [-47.92972, -15.77972], radius: 12 },
    { name: "São Paulo", coordinates: [-46.625290, -23.533773], radius: 20 },
    { name: "Londres", coordinates: [-0.1277583, 51.5073509], radius: 10 },
    { name: "Maranhão", coordinates: [-44.3044, -2.5283], radius: 6 },
    { name: "Sydney", coordinates: [151.209900, -33.865143], radius: 4 },
    { name: "New York City", coordinates: [-74.0059413, 40.7127837], radius: 6 }
]

const MapChart = ({ countries }) => {
    const _ctr = countries[0]
    const _country = Object.keys(_ctr)[0]
    const max = _ctr[_country].total
    const max_radius = 35
    return (
        <ComposableMap>
            <Geographies geography={geoMap}>
                {({ geographies }) =>
                    geographies.map(geo => <Geography key={geo.rsmKey} geography={geo} fill="#EAEAEC" stroke="#D6D6DA" />)
                }
            </Geographies>
            
            {countries.map(ctr => {
                const country = Object.keys(ctr)[0]
                if (country !== ''){
                    const radius = Math.min(1000*ctr[country].total/max, max_radius) 
                    return (
                        <Marker key={country} coordinates={Array(ctr[country].long, ctr[country].lat)}>
                            <circle r={radius} fill="#1F1BC6"  opacity="0.25" />
                        </Marker>
                    )
                }
            })}
        </ComposableMap>
    )
}

export default MapChart