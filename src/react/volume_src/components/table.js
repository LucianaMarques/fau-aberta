import React from 'react'
import styled from 'styled-components'

function Table({ title1, title2, col1, col2 }) {
    const entries = col1.reduce((prev, cur, id) => {
        return [...prev, {c1: cur, c2: col2[id]}]
    }, [])
    console.log('Entries: ', entries)
    return (
        <Column>
            <Row>
                <span style={{backgroundColor: '#323232', color: 'white'}}>{title1}</span>
                <span style={{backgroundColor: '#323232', color: 'white'}}>{title2}</span>
            </Row>
            {entries.map(entry => {
                return (
                    <Row>
                        <span>{entry.c1}</span>
                        <span>{entry.c2}</span>
                    </Row>
                )
            })}
        </Column>
    )
}

export default Table

const Row = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    border: 1px solid #323232;
    border-bottom: 0;
    > span {
        :nth-child(1) { width: 350px }
        :nth-child(2) { width: 200px }
        text-align: center;
        padding: 8px 0px 8px;
        font-size: 18px;
        background-color: #E5E5E5;
        color: black;
        border-bottom: 1px solid #323232;
    }
    :hover > span {
        background-color: #C2C2C2;
    }
`

const Column = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 550px;
    margin-bottom: 32px;
`
