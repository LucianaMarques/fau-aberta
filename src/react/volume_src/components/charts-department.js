import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import * as S from '../styles'
import Selector from './selector'
import 'rc-slider/assets/index.css'
import LineChart from '../charts/line'
import Range from 'rc-slider/lib/Range'
import Table from './table'
import Loading from './loading'
import { dict } from '../dict'

const MIN_YEAR = 1978
const MAX_YEAR = 2019

function ChartsDepartment({ title, department, getCount, getTipos, getRank }) {
    const [yearsFiltered, setYearsFiltered] = useState([1990, 2018])
    const [lineData, setLineData] = useState(null)
    const [rankData, setRankData] = useState(null)
    const [cachedLineData, setCachedLineData] = useState(null)
    const [allCategories, setAllCategories] = useState([])
    const [categories, setCategories] = useState([])
    const [notSelected, setNotSelected] = useState([])
    const [loadingLine, setLoadingLine] = useState(true)
    const [loadingRank, setLoadingRank] = useState(true)

    useEffect(() => {
        const departments = [department]

        getCount({ ano_inicio: 1978 }).then(res => {
            setCachedLineData(res[department])
        })

        getRank({ departamentos: departments}).then(res => {
            let docentes = []
            let qtds = []
            res.forEach(obj => {
                const keys = Object.keys(obj)
                
                docentes.push(obj[keys[0]])
                qtds.push(obj[keys[1]])
            })
            setRankData({ docentes, qtds })
            setLoadingRank(false)
        })

        getTipos({ departamentos: departments }).then(res => {
            setAllCategories(res)
        })
    }, [])

    useEffect(() => {
        setCategories(allCategories.slice(0, 4))
        setNotSelected(allCategories.slice(4))
    }, [allCategories])

    useEffect(() => {
        const params = {
            departamentos: [department],
            ano_inicio: yearsFiltered[0],
            ano_fim: yearsFiltered[1],
            tipos: categories
        }
        setLoadingRank(true)

        getRank(params).then(res => {
            let docentes = []
            let qtds = []
            res.forEach(obj => {
                const keys = Object.keys(obj)

                docentes.push(obj[keys[0]])
                qtds.push(obj[keys[1]])
            })
            setRankData({docentes, qtds})
            setLoadingRank(false)
        })

    }, [yearsFiltered, categories])

    useEffect(() => {
        let data = {
            labels: [],
            datasets: []
        }
        const minYear = yearsFiltered[0], maxYear = yearsFiltered[1]
        const years = Array(maxYear - minYear + 1).fill()

        if (!cachedLineData)
            return
        setLoadingLine(true)
        years.forEach((_, i) => {
            const year = minYear + i
            data.labels = [...data.labels, year]
        })
        categories.forEach((category, i) => {
            let dataset = {
                label: dict(category),
                data: [],
                fill: false,
                borderColor: S.categoriesColors[i],
                backgroundColor: S.categoriesColors[i]
            }
            years.forEach((_, i) => {
                dataset.data = [...dataset.data, cachedLineData[category][minYear + i]]
            })
            data.datasets = [...data.datasets, dataset]
        })
        setLineData(data)
        setLoadingLine(false)
    }, [categories, cachedLineData, yearsFiltered])

    return (
        <S.CenteredColumn>
            <S.Title style={{marginTop: 48}}>{title.toUpperCase()} {department.toUpperCase()}</S.Title>

            <S.SubTitle>Selecione o período e o(s) tipo(s) desejados</S.SubTitle>
            <Slider style={{paddingTop: 0}}>
                <span>{MIN_YEAR}</span>
                <Range min={MIN_YEAR} max={MAX_YEAR} defaultValue={[yearsFiltered[0], yearsFiltered[1]]} 
                        allowCross={false} onChange={setYearsFiltered} />
                <span>{MAX_YEAR}</span>
            </Slider>

            <Years>
                <span>Do ano</span>
                <span>{yearsFiltered[0]}</span>
                <span>ao</span>
                <span>{yearsFiltered[1]}</span>
            </Years>

            {loadingLine ?
                <div style={{ width: 750, height: 440, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <Loading />
                </div>
                :
                <S.Row>
                    <LineChart formatedData={lineData} />
                    <Selector categories={categories} setCategories={setCategories} notSelected={notSelected} setNotSelected={setNotSelected} />
                </S.Row>
            }

            <S.SubTitle>Docentes que mais produziram estes tipos no período selecionado</S.SubTitle>
            {loadingRank ?
                <div style={{ width: 750, height: 440, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <Loading />
                </div>
                :
                <Table title1="Nome Docente" title2="Qtd. de Produções" col1={rankData.docentes} col2={rankData.qtds} />
            }
            
        </S.CenteredColumn>
    )
}

export default ChartsDepartment

const Slider = styled.div`
    width: 350px;
    padding-top: 48px;
    display: flex;
    flex-direction: row;
    > span {
        :nth-child(1) { padding-right: 12px }
        :nth-child(3) { padding-left: 12px }
        font-size: 14px;
        margin-top: -3px;
        opacity: 0.5;
    }
`
const Years = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 8px 0px 24px;
    font-size: 20px;
    > span {
        :nth-child(2) {
            font-weight: 900;
            padding: 0px 16px 0px 16px;
        }
        :nth-child(4) {
            font-weight: 900;
            padding-left: 16px;
        }
    }
`