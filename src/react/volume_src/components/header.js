import React from 'react'
import { Link } from 'react-router-dom'
import LogoFAU from '../assets/logo-fau-invertido.png'
import styled from 'styled-components'
import { primary, secondary } from '../styles'

function Header() {
    return (
        <Container>
            <Text>
                <Link to='/'>
                    <div className="title">FAU ABERTA</div>
                    <div className="sub-title">Produção Intelectual da FAU-USP</div>
                </Link>
            </Text>
            <Link to='/'><img src={LogoFAU} width={220} height={95} style={{marginTop: 4}} alt="Logo FAU" /></Link>
        </Container>
    )
}

export default Header

const Container = styled.div`
    display: flex;
    justify-content: space-between;
    position: fixed;
    top: 0;
    width: 100%;
    height: 106px;
    background-color: ${primary};
    border-bottom: 7px solid ${secondary};
    padding: 12px 36px 12px 36px;
    font-family: 'Source Sans Pro', sans-serif;
    z-index: 9999;
    > * img { margin-right: 72px }
`
const Text = styled.div`
    .title { 
        font-size: 48px;
        margin-bottom: 8px;
        color: white !important;
        font-weight: 900;
    }
    .sub-title { 
        font-size: 26px;
        color: white !important;
    }
    > a {
        text-decoration: none !important;
    }
`