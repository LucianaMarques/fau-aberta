import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import * as S from '../styles'
import StackedBar from '../charts/bar'
import Selector from './selector'
import 'rc-slider/assets/index.css'
import LineChart from '../charts/line'
import WordCloud from '../components/word-cloud'
import MapChart from './world-map'
import FAU from '../assets/fau.jpg'
import { Link } from 'react-router-dom'
import { dict } from '../dict'
import Loading from './loading'

function ChartsOverview({ title, getIndex, getCount, getKeywords, getTipos, getCountTipos, getMap, description = null }) {
    const [allCategories, setAllCategories] = useState([])
    const [categories, setCategories] = useState([])
    const [notSelected, setNotSelected] = useState([])
    const [barData, setBarData] = useState(null)
    const [words, setWords] = useState([])
    const [lineData, setLineData] = useState(null)
    const [allCount, setAllCount] = useState(null)
    const [map, setMap] = useState(null)
    const [loadingBar, setLoadingBar] = useState(true)
    const [loadingLine, setLoadingLine] = useState(true)
    const [loadingWords, setLoadingWords] = useState(true)

    useEffect(() => {
        setCategories(allCategories.slice(0,4))
        setNotSelected(allCategories.slice(4,))
    }, [allCategories])
    
    useEffect(() => {
        let data = {
            labels: ['AUH', 'AUT', 'AUP'],
            datasets: categories.map((category, index) => {
                return {
                    label: dict(category),
                    backgroundColor: S.categoriesColors[index%15],
                    data: [],
                    value: category
                }
            })
        }
        
        data.datasets.forEach(obj => {
            data.labels.forEach(dep => {
                obj.data.push(Object.values(allCount[dep][obj.value]).reduce((a, b) => a + b))
            })
        })
        setBarData(data)
    }, [categories])

    useEffect(() => {
        let data = {}
        const departments = ['AUH', 'AUT', 'AUP']
        getCount({ ano_inicio: 1978 }).then(res => {
            departments.forEach(dep => {
                data[dep] =  Object.values(res[dep]).reduce((prev, curr) => {
                    for (let key in curr) {
                        prev[key] = (prev[key] || 0) + curr[key];
                    }
                    return prev
                }, {});
            })
            setLineData(data)
            setLoadingLine(false)
        })
        getTipos().then(res => {
            getCount({ tipos: res }).then(count => {
                setAllCount(count)
                setAllCategories(res)
                setLoadingBar(false)
            })
        })
        if (getMap) getMap().then(res => setMap(res))
    }, [])

    useEffect(() => {
        const limit = 100

        getKeywords({ limit }).then(res => {
            setWords(res.map(obj => {
                return {
                    text: Object.keys(obj)[0],
                    value: Object.values(obj)[0]
                }
            }))
            setLoadingWords(false)
        })
    }, [])

    return (
        <S.CenteredColumn style={{paddingTop: 36}}>
            <S.Title>{title.toUpperCase()}</S.Title>

            {description &&
                <S.TextArea>
                    <S.TextBox style={{marginBottom: 32}}>
                        <span>{description}</span>
                    </S.TextBox>
                </S.TextArea>
            }

            <S.SubTitle>
                {`Distribuição d${title.substr(0, 1) === 'P' ? 'a' : 'e'} ${title.toLowerCase()} por departamento`}
            </S.SubTitle>

            <S.Row style={{alignItems: 'center'}}>
                {loadingBar ?
                    <div style={{width: 750, height: 400, display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                        <Loading />
                    </div>
                    :
                    <React.Fragment>
                        <div style={{width: 750, height: 400}}>
                            <StackedBar data={barData} />
                        </div>
                        <Selector categories={categories} setCategories={setCategories} 
                                notSelected={notSelected} setNotSelected={setNotSelected} />
                    </React.Fragment>
                }
            </S.Row>
            
            {loadingLine ? 
                <div style={{width: 750, height: 440, display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                    <Loading />
                </div>
                :
                <LineChart cachedData={lineData} slider={true} />
            }

            <S.SubTitle style={{marginTop: 80}}>Palavras mais utilizadas nos títulos</S.SubTitle>

            <CloudContainer>
                {loadingWords ?
                    <Loading />
                    :
                    <WordCloud words={words} />
                }
            </CloudContainer>


            {map && 
                <React.Fragment>
                    <S.SubTitle style={{marginBottom: 0}}>Produção nacional e internacional</S.SubTitle>
                    <MapContainer> 
                        <MapChart countries={map}/>
                    </MapContainer>
                </React.Fragment>
            }

            <S.SubTitle>Clique abaixo para ver produções bibliográficas por departamento</S.SubTitle>

            <Boxes>
                <FAUBackground src={FAU} />
                <Link to={`${window.location.pathname}-auh`}><Box>AUH</Box></Link>
                <Link to={`${window.location.pathname}-aut`}><Box>AUT</Box></Link>
                <Link to={`${window.location.pathname}-aup`}><Box>AUP</Box></Link>
            </Boxes>

        </S.CenteredColumn>

    )
}

export default ChartsOverview

const CloudContainer = styled.div`
    width: 750px;
    height: 350px;
    margin: 16px 0px 24px;
    display: flex;
    justify-content: center;
    align-items: center;
`
const MapContainer = styled.div`
    width: 800px;
    height: 600px;
`
const Boxes = styled.div`
    width: 100%;
    height: 400px;
    display: flex;
    justify-content: space-around;
    align-items: center;
    > a {
        text-decoration: none !important;
        color: black !important;
    }
`
const Box = styled.div`
    border: 3px solid #969696;
    background-color: rgba(255, 255, 255, 0.3);
    border-radius: 4px;
    width: 250px;
    height: 250px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 78px;
    font-weight: 900;
    transition: background-color 0.5s;
    :hover {
        background-color: rgba(255, 255, 255, 0.7);
    }
`
const FAUBackground = styled.img`
    position: absolute;
    width: 100%;
    height: 400px;
    opacity: 0.3;
    z-index: -1;
`