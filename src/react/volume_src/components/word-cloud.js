import React from 'react'
import ReactWordCloud from 'react-wordcloud'

const options = {
    colors: ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b'],
    enableTooltip: true,
    deterministic: false,
    fontFamily: 'impact',
    fontSizes: [5, 60],
    fontStyle: 'normal',
    fontWeight: 'normal',
    padding: 1,
    rotations: 2,
    rotationAngles: [0, 270],
    scale: 'sqrt',
    spiral: 'archimedean',
    transitionDuration: 1000,
}

function WordCloud({ words }) {
    return (
        <ReactWordCloud options={options} words={words} />
    )
}

export default WordCloud