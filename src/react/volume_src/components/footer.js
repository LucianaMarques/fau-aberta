import React from 'react'
import LogoFAUInvertido from '../assets/logo-fau-invertido.png'
import LogoIMEInvertido from '../assets/logo-ime-invertido.png'
import LogoCNPQInvertido from '../assets/logo-cnpq-invertido.png'
import styled from 'styled-components'
import { secondary } from '../styles'
import { Link } from 'react-router-dom'

function Footer() {
    return (
        <Container>
            <List>
                <a href="http://www.fau.usp.br/" target="_blank" rel="noopener noreferrer">
                    <Image src={LogoFAUInvertido} />
                </a>
                <span>Faculdade de Arquitetura e Urbanismo</span>
                <span>R. do Lago, 876 - Butantã, São Paulo - SP, 05508-080</span>
                <span>(11) 3091-4795</span>
            </List>
            <List style={{justifyContent: 'space-around'}}>
                <a href="http://www.cnpq.br/" target="_blank" rel="noopener noreferrer">
                    <Image style={{height: 42}} src={LogoCNPQInvertido} />
                </a>
                <Link to='/sobre-nos'>Sobre o projeto</Link>
            </List>
            <List>
                <a href="http://www.ime.usp.br/" target="_blank" rel="noopener noreferrer">
                    <Image src={LogoIMEInvertido} />
                </a>
                <span>Instituto de Matemática e Estatística</span>
                <span>R. do Matão, 1010 - Butantã, São Paulo - SP, 05508-090</span>
                <span>(11) 3091-6101</span>
            </List>
        </Container>
    )
}

export default Footer

const Container = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 24px;
    background-color: ${secondary};
    color: white;
    height: 180px;
    width: calc(100% - 96px);
    padding: 24px 48px 16px 48px;
    font-family: 'Source Sans Pro', sans-serif;
`
const List = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    color: white;
    > :not(:last-child) {
        margin-bottom: 4px;
        width: 300px;
        text-align: center;
    }
    > a {
        text-decoration: none;
        color: white;
        font-size: 20px;
    }
`
const Image = styled.img`
    width: 150px;
    height: 65px;
    cursor: pointer;
    margin-bottom: 16px;
`