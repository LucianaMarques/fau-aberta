import React from 'react'
import SolFau from '../assets/sol-fau.png'
import './css/loading.scss'

function Loading() {
    return <img src={SolFau} className='loading-spinner-fau' />
}

export default Loading