const ProdBib = 'Nesta página serão expostos os dados sobre produção bibliográfica de cada departamento da FAU, que envolvem publicações em periódicos credenciados, livros, capítulos de livros, anais de eventos, resumos em anais de eventos, prefácios/ posfácios, traduções, jornais e outros.'
const ProdTec = 'Nesta página serão expostos os dados sobre a produção técnica de cada departamento da FAU, que envolvem organização de evento, apresentação de trabalho, participação em programa de rádio ou TV, projetos e produtos de desenho industrial, relatórios de pesquisa, desenvolvimento de material didático ou institucional, produtos tecnológicos, trabalhos técnicos, mídias sociais/ websites/ blogs, maquetes, editoração, cursos de curta duração ministrados, patentes, processos ou técnicas, softwares, cartas ou mapas similares, manutenção de obras artísticas e outros.'
const ProdArt = 'Nesta página serão expostos os dados sobre a produção artística de cada departamento da FAU, que envolvem obras de artes visuais, cursos de curta duração e demais atividades em música, artes cênicas e artes visuais.'

export {
    ProdBib,
    ProdTec,
    ProdArt
}