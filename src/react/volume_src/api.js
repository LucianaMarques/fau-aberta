import axios from 'axios'

const url = 'http://localhost:8000'

const fetchApi = (base, params={}) => {
    var request = url + base
    return axios.get(request, { params } ).then(res => {
        if (res.status !== 200) return null
        return res.data.result
    })
}

const getDashboard = () => {
    return fetchApi('/dashboard/').then(data => {
        if (data)
            return data
    })
}

const getProdBib = (params) => {
    return fetchApi('/producao_bibliografica', params).then(data => {
        return data
    })
}

const getProdBibCount = (params) => {
    return fetchApi('/producao_bibliografica/count', params).then(data => {
        return data
    })
}

const getProdBibCountTipos = (params) => {
    return fetchApi('/producao_bibliografica/count_tipos', params).then(data => {
        return data
    })
}

const getProdBibKeywords = (params) => {
    return fetchApi('/producao_bibliografica/keywords', params).then(data => {
        return data
    })
}

const getProdBibMap = (params) => {
    return fetchApi('/producao_bibliografica/map', params).then(data => {
        return data
    })
}

const getProdBibRank = (params) => {
    return fetchApi('/producao_bibliografica/rank', params).then(data => {
        return data
    })
}

const getProdBibTipos = () => {
    return fetchApi('/producao_bibliografica/tipos').then(data => {
        return data
    })
}

const getProdArtMap = () => {
    return fetchApi('/producao_bibliografica/map').then(data => {
        return data
    })
}

const getProdTec = (params) => {
    return fetchApi('/producao_tecnica', params).then(data => {
        return data
    })
}

const getProdTecCount = (params) => {
    return fetchApi('/producao_tecnica/count', params).then(data => {
        return data
    })
}

const getProdTecCountTipos = (params) => {
    return fetchApi('/producao_tecnica/count_tipos', params).then(data => {
        return data
    })
}

const getProdTecKeywords = (params) => {
    return fetchApi('/producao_tecnica/keywords', params).then(data => {
        return data
    })
}

const getProdTecMap = (params) => {
    return fetchApi('/producao_tecnica/map', params).then(data => {
        return data
    })
}

const getProdTecRank = (params) => {
    return fetchApi('/producao_tecnica/rank', params).then(data => {
        return data
    })
}

const getProdTecTipos = () => {
    return fetchApi('/producao_tecnica/tipos').then(data => {
        return data
    })
}

const getProdArt = (params) => {
    return fetchApi('/producao_artistica', params).then(data => {
        return data
    })
}

const getProdArtCount = (params) => {
    return fetchApi('/producao_artistica/count', params).then(data => {
        return data
    })
}

const getProdArtCountTipos = (params) => {
    return fetchApi('/producao_artistica/count_tipos', params).then(data => {
        return data
    })
}

const getProdArtKeywords = (params) => {
    return fetchApi('/producao_artistica/keywords', params).then(data => {
        return data
    })
}

const getProdArtRank = (params) => {
    return fetchApi('/producao_artistica/rank', params).then(data => {
        return data
    })
}

const getProdArtTipos = () => {
    return fetchApi('/producao_artistica/tipos').then(data => {
        return data
    })
}

const getOrientacao = () => {
    return fetchApi('/orientacao').then(data => {
        return data
    })
}

const getOrientacaoCount = (params) => {
    return fetchApi('/orientacao/count', params).then(data => {
        return data
    })
}

const getOrientacaoCountTipos = (params) => {
    return fetchApi('/orientacao/count_tipos', params).then(data => {
        return data
    })
}

const getOrientacaoKeywords = (params) => {
    return fetchApi('/orientacao/keywords', params).then(data => {
        return data
    })
}

const getOrientacaoMap = () => {
    return fetchApi('/orientacao/map').then(data => {
        return data
    })
}

const getOrientacaoRank = (params) => {
    return fetchApi('/orientacao/rank', params).then(data => {
        return data
    })
}

const getOrientacaoTipos = () => {
    return fetchApi('/orientacao/tipos').then(data => {
        return data
    })
}

const getBancas = () => {
    return fetchApi('/Bancas').then(data => {
        return data
    })
}

const getBancasCount = (params) => {
    return fetchApi('/bancas/count', params).then(data => {
        return data
    })
}

const getBancasCountTipos = (params) => {
    return fetchApi('/bancas/count_tipos', params).then(data => {
        return data
    })
}

const getBancasKeywords = (params) => {
    return fetchApi('/bancas/keywords', params).then(data => {
        return data
    })
}

const getBancasMap = () => {
    return fetchApi('/bancas/map').then(data => {
        return data
    })
}

const getBancasRank = (params) => {
    return fetchApi('/bancas/rank', params).then(data => {
        return data
    })
}

const getBancasTipos = () => {
    return fetchApi('/bancas/tipos').then(data => {
        return data
    })
}

const getPremiosRank = (params) => {
    return fetchApi('/premios/rank', params).then(data => {
        return data
    })
}

export {
    getDashboard,
    getProdBib,
    getProdBibCount,
    getProdBibCountTipos,
    getProdBibKeywords,
    getProdBibMap,
    getProdBibRank,
    getProdBibTipos,
    getProdTec,
    getProdTecCount,
    getProdTecCountTipos,
    getProdTecKeywords,
    getProdTecMap,
    getProdTecRank,
    getProdTecTipos,
    getProdArt,
    getProdArtCount,
    getProdArtCountTipos,
    getProdArtKeywords,
    getProdArtMap,
    getProdArtRank,
    getProdArtTipos,
    getOrientacao,
    getOrientacaoCount,
    getOrientacaoCountTipos,
    getOrientacaoKeywords,
    getOrientacaoMap,
    getOrientacaoRank,
    getOrientacaoTipos,
    getBancas,
    getBancasCount,
    getBancasCountTipos,
    getBancasKeywords,
    getBancasMap,
    getBancasRank,
    getBancasTipos,
    getPremiosRank
}