import React from 'react'
import { Bar } from 'react-chartjs-2'

function StackedBar({data = null}) {
    const options = {
        animation: {
            duration: 1500
        },
        scales: {
            xAxes: [{
                barPercentage: 0.5,
                stacked: true,
                gridLines: { display: false }
            }],
            yAxes: [{
                stacked: true
            }]
        },
        legend: { 
            display: true,
            labels: {
                padding: 40,
                boxWidth: 20
            }
        },
        maintainAspectRatio: true,
        responsive: false,
    }

    return data && <Bar data={data} options={options} width={750} height={400} />
}

export default StackedBar
