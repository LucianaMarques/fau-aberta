import React from 'react'
import { Pie } from 'react-chartjs-2'

function PieChart({data = null, legend = true}) {
    const options = {
        legend: { display: legend },
        maintainAspectRatio: true,
        responsive: false
    }

    return data && <Pie data={data} options={options} width={500} height={300} />
}

export default PieChart