import React, { useState, useEffect } from 'react'
import { Line } from 'react-chartjs-2'
import styled from 'styled-components'
import Range from 'rc-slider/lib/Range'
import * as S from '../styles'

const MIN_YEAR = 1978
const MAX_YEAR = 2019

function LineChart({ cachedData = null, formatedData = null, slider = false}) {
    const [yearsFiltered, setYearsFiltered] = useState([1990, 2018])
    const [lineData, setLineData] = useState(null)

    useEffect(() => {
        if (formatedData != null) {
            setLineData(formatedData)
            return
        }
        let data = {
            labels: [],
            datasets: []
        }
        const minYear = yearsFiltered[0], maxYear = yearsFiltered[1]
        const years = Array(maxYear - minYear + 1).fill()
        years.forEach((_, i) => {
            const year = minYear + i
            data.labels = [...data.labels, year]
        })
        const departments = ['AUH', 'AUT', 'AUP']
        departments.forEach(department => {
            let dataset = {
                label: department,
                data: [],
                fill: false,
                borderColor: S.departmentColors[department],
                backgroundColor: S.departmentColors[department]
            }
            years.forEach((_, i) => {
                if (cachedData)
                    dataset.data = [...dataset.data, cachedData[department][minYear+i]]
                else
                    dataset.data = [...dataset.data, 0]
            })
            data.datasets = [...data.datasets, dataset]
        })

        setLineData(data)
    }, [yearsFiltered, cachedData, formatedData])

    const options = {
        legend: { display: true },
        maintainAspectRatio: true,
        responsive: false
    }

    return lineData && (
        <S.CenteredColumn>
            {slider &&
                <div>
                    <Slider>
                        <span>{MIN_YEAR}</span>
                        <Range min={MIN_YEAR} max={MAX_YEAR} defaultValue={[yearsFiltered[0], yearsFiltered[1]]} 
                                allowCross={false} onChange={setYearsFiltered} />
                        <span>{MAX_YEAR}</span>
                    </Slider>

                    <Years>
                        <span>Do ano</span>
                        <span>{yearsFiltered[0]}</span>
                        <span>ao</span>
                        <span>{yearsFiltered[1]}</span>
                    </Years>
                </div>
            }

            <Line data={lineData} options={options} width={750} height={320} />
        </S.CenteredColumn>
    )
}

export default LineChart

const Slider = styled.div`
    width: 350px;
    padding-top: 48px;
    display: flex;
    flex-direction: row;
    > span {
        :nth-child(1) { padding-right: 12px }
        :nth-child(3) { padding-left: 12px }
        font-size: 14px;
        margin-top: -3px;
        opacity: 0.5;
    }
`
const Years = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 8px 0px 24px;
    font-size: 20px;
    > span {
        :nth-child(2) {
            font-weight: 900;
            padding: 0px 16px 0px 16px;
        }
        :nth-child(4) {
            font-weight: 900;
            padding-left: 16px;
        }
    }
`