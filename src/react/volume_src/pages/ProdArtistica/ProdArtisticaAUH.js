import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getProdArtCount, getProdArtTipos, getProdArtRank } from '../../api'

function ProdArtisticaAUH() {
    return (
        <ChartsDepartment
            title="Produção Artística"
            department="AUH"
            getCount={getProdArtCount}
            getTipos={getProdArtTipos}
            getRank={getProdArtRank}
        />
    )
}

export default ProdArtisticaAUH