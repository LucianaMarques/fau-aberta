import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getProdArtCount, getProdArtTipos, getProdArtRank } from '../../api'

function ProdArtisticaAUT() {
    return (
        <ChartsDepartment
            title="Produção Artística"
            department="AUT"
            getCount={getProdArtCount}
            getTipos={getProdArtTipos}
            getRank={getProdArtRank}
        />
    )
}

export default ProdArtisticaAUT