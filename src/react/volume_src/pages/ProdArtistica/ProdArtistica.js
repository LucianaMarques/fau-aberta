import React from 'react'
import { getProdArt, getProdArtCount, getProdArtKeywords, getProdArtTipos, getProdArtMap } from '../../api'
import ChartsOverview from '../../components/charts-overview'
import { ProdArt } from '../../static_texts'

function ProdArtistica() {
    return (
        <ChartsOverview
            title="Produção Artística"
            getIndex={getProdArt}
            getCount={getProdArtCount}
            getKeywords={getProdArtKeywords}
            getTipos={getProdArtTipos}
            getMap={getProdArtMap}
            description={ProdArt}
        />
    )
}

export default ProdArtistica