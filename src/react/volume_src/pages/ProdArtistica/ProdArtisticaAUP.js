import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getProdArtCount, getProdArtTipos, getProdArtRank } from '../../api'

function ProdArtisticaAUP() {
    return (
        <ChartsDepartment
            title="Produção Artística"
            department="AUP"
            getCount={getProdArtCount}
            getTipos={getProdArtTipos}
            getRank={getProdArtRank}
        />
    )
}

export default ProdArtisticaAUP