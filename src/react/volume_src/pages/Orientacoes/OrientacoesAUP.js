import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getOrientacaoCount, getOrientacaoTipos, getOrientacaoRank } from '../../api'

function OrientacoesAUP() {
    return (
        <ChartsDepartment
            title="Orientações"
            department="AUP"
            getCount={getOrientacaoCount}
            getTipos={getOrientacaoTipos}
            getRank={getOrientacaoRank}
        />
    )
}

export default OrientacoesAUP