import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getOrientacaoCount, getOrientacaoTipos, getOrientacaoRank } from '../../api'

function OrientacoesAUT() {
    return (
        <ChartsDepartment
            title="Orientações"
            department="AUT"
            getCount={getOrientacaoCount}
            getTipos={getOrientacaoTipos}
            getRank={getOrientacaoRank}
        />
    )
}

export default OrientacoesAUT