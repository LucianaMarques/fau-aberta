import React from 'react'
import { getOrientacao, getOrientacaoCount, getOrientacaoKeywords, getOrientacaoTipos } from '../../api'
import ChartsOverview from '../../components/charts-overview'

function Orientacoes() {
    return (
        <ChartsOverview
            title="Orientações"
            getIndex={getOrientacao}
            getCount={getOrientacaoCount}
            getKeywords={getOrientacaoKeywords}
            getTipos={getOrientacaoTipos}
        />
    )
}

export default Orientacoes