import React from 'react'
import { getBancas, getBancasCount, getBancasKeywords, getBancasTipos } from '../../api'
import ChartsOverview from '../../components/charts-overview'

function Bancas() {
    return (
        <ChartsOverview 
            title="Bancas"
            getIndex={getBancas}
            getCount={getBancasCount}
            getKeywords={getBancasKeywords}
            getTipos={getBancasTipos}
        />
    )
}

export default Bancas