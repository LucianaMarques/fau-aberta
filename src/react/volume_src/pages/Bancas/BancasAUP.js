import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getBancasCount, getBancasTipos, getBancasRank } from '../../api'

function BancasAUP() {
    return (
        <ChartsDepartment 
            title="Bancas"
            department="AUP"
            getCount={getBancasCount}
            getTipos={getBancasTipos}
            getRank={getBancasRank} 
        />
    )
}

export default BancasAUP