import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getBancasCount, getBancasTipos, getBancasRank } from '../../api'

function BancasAUT() {
    return (
        <ChartsDepartment
            title="Bancas"
            department="AUT"
            getCount={getBancasCount}
            getTipos={getBancasTipos}
            getRank={getBancasRank}
        />
    )
}

export default BancasAUT