import React from 'react'
import { getProdBib, getProdBibCount, getProdBibKeywords, getProdBibTipos, getProdBibMap } from '../../api'
import ChartsOverview from '../../components/charts-overview'
import { ProdBib } from '../../static_texts'

function ProdBibliografica() {
    return (
        <ChartsOverview 
            title="Produção Bibliográfica" 
            getIndex={getProdBib}
            getCount={getProdBibCount}
            getKeywords={getProdBibKeywords}
            getTipos={getProdBibTipos}
            getMap={getProdBibMap}
            description={ProdBib}
        />
    )
}

export default ProdBibliografica