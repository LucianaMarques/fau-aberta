import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getProdBibCount, getProdBibTipos, getProdBibRank } from '../../api'

function ProdBibliograficaAUH() {
    return (
        <ChartsDepartment
            title="Produção Bibliográfica"
            department="AUH"
            getCount={getProdBibCount}
            getTipos={getProdBibTipos}
            getRank={getProdBibRank}
        />
    )
}

export default ProdBibliograficaAUH