import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getProdBibCount, getProdBibTipos, getProdBibRank } from '../../api'

function ProdBibliograficaAUP() {
    return (
        <ChartsDepartment
            title="Produção Bibliográfica"
            department="AUP"
            getCount={getProdBibCount}
            getTipos={getProdBibTipos}
            getRank={getProdBibRank}
        />
    )
}

export default ProdBibliograficaAUP