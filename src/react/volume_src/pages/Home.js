import React, { useState, useEffect } from 'react'
import * as S from '../styles'
import {getDashboard} from '../api.js'
import styled from 'styled-components'
import FAU from '../assets/fau.jpg'
import { Link } from 'react-router-dom'

const colors = {
    'Produção Bibliográfica': 'rgba(252, 117, 33, 0.9)',
    'Produção Artística': 'rgba(58, 94, 3, 0.9)',
    'Produção Técnica': 'rgba(107, 51, 20, 0.9)',
    'Orientação': 'rgba(34, 172, 227, 0.9)',
    'Bancas': 'rgba(40, 4, 79, 0.9)',
    'Prêmios e Títulos': 'rgba(63, 150, 158, 0.9)'
}

const links = {
    'Produção Bibliográfica': '/producao-bibliografica',
    'Produção Artística': 'producao-artistica',
    'Produção Técnica': 'producao-tecnica',
    'Orientação': 'orientacao',
    'Bancas': 'bancas',
    'Prêmios e Títulos': 'premios'
}

function Home() {
    const [dashboard, setDashboard] = useState({})

    useEffect(() => {
        getDashboard().then(res => {
            setDashboard(res)
        })
    }, [])

    return (
        <S.CenteredColumn style={{justifyContent: 'start', paddingTop: 36}}>
            <S.TextArea>
                <About>
                    <span>O que é a FAU Aberta?</span>
                </About>

                <S.TextBox>
                    <span>
                        A FAU ABERTA é uma iniciativa da Comissão de Pesquisa da FAUUSP
                        (CPq-FAUUSP) com o intuito de dar visibilidade à produção intelectual
                        da faculdade, difundindo sua especificidade e diversidade através de
                        dados extraídos do currículo Lattes de docentes e alunos. Destinado à
                        comunidade acadêmica em geral, a FAU ABERTA resulta da cooperação
                        entre a FAU e o Instituto de Matemática e Estatística (IME-USP),
                        representado por um grupo de alunos da disciplina Laboratório de
                        Programação Extrema (Extreme Programming Lab) (MAC0342), coordenada
                        pelo Prof. Dr. Alfredo Goldman Vel Lejbman, para desenvolver uma base
                        de dados automatizada que alimentará a plataforma web de acesso
                        aberto. O projeto envolveu a participação de docentes de ambas às
                        unidades, discentes de graduação, bibliotecários e a Superintendência
                        da Tecnologia e Informação da USP (STI-USP).
                    </span>
                    <span>
                        É compromisso das
                        universidades públicas ampliar o acesso às suas pesquisas acadêmicas e
                        aos seus resultados (produções bibliográficas, técnicas e artísticas),
                        permitindo quantificá-los e qualificá-los em termos de impacto social,
                        impacto econômico, inovação tecnológica e desdobramentos em políticas
                        públicas e de sustentabilidade. A primeira etapa do projeto se
                        apresenta como um piloto, disponibilizando apenas a produção
                        intelectual dos docentes da FAUUSP cadastrada até setembro de 2019,
                        quando os dados foram coletados. Para a segunda etapa de automatização
                        do sistema, contaremos com o apoio do STI-USP, STI-FAU e Diretoria da
                        FAUUSP.
                    </span>
                </S.TextBox>
            </S.TextArea>

            <Body>
                <FAUBackground src={FAU} />
                <S.CenteredColumn>
                    <Overview>
                        {Object.keys(dashboard).map(key => <Info key={key} title={key} value={dashboard[key]} />)}
                    </Overview>
                </S.CenteredColumn>
            </Body>
        </S.CenteredColumn>
    )
}

export default Home


const Info = ({title, value}) => {
    return (
        <Information style={{backgroundColor: colors[title], color: 'white'}}>
            <Link to={links[title]}>
                <span>{value}</span>
                <span>{title}</span>
            </Link>
        </Information>
    )
}

const Overview = styled.div`
    width: 100%;
    max-width: 1440px;
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-row-gap: 64px;
    grid-column-gap: 144px;
`
const Information = styled.div`
    border-radius: 78px;
    width: 320px;
    height: 320px;
    transition: all 0.2s ease-in-out;
    :hover { transform: scale(1.1); }
    > a {
        align-self: center;
        justify-self: center;
        display: flex;
        align-items: center;
        flex-direction: column;
        cursor: pointer;
        color: white;
        text-decoration: none;
        width: 100%;
        height: 100%;

        > span {
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            width: 160px;
            opacity: 1;
            :first-child {
                font-size: 106px;
                position: relative;
                top: 64px;
            }
            :nth-child(2) {
                height: 50px;
                font-size: 22px;
                position: relative;
                top: 96px;
            }
        }
    }
`
const About = styled.div`
    > span { font-size: 30px }
    display: flex;
    justify-content: flex-start;
    padding-left: 2px;
`
const FAUBackground = styled.img`
    position: absolute;
    width: 100%;
    height: 900px;
    opacity: 0.3;
    z-index: -1;
`
const Body = styled.div`
    width: 100%;
    margin: 72px 0px 48px;
    height: 900px;
`