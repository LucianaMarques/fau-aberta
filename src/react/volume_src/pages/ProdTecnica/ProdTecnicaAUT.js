import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getProdTecCount, getProdTecTipos, getProdTecRank } from '../../api'

function ProdTecnicaAUT() {
    return (
        <ChartsDepartment
            title="Produção Técnica"
            department="AUT"
            getCount={getProdTecCount}
            getTipos={getProdTecTipos}
            getRank={getProdTecRank}
        />
    )
}

export default ProdTecnicaAUT