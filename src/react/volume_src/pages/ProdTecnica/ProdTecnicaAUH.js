import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getProdTecCount, getProdTecTipos, getProdTecRank } from '../../api'

function ProdTecnicaAUH() {
    return (
        <ChartsDepartment
            title="Produção Técnica"
            department="AUH"
            getCount={getProdTecCount}
            getTipos={getProdTecTipos}
            getRank={getProdTecRank}
        />
    )
}

export default ProdTecnicaAUH