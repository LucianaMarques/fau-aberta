import React from 'react'
import { getProdTec, getProdTecCount, getProdTecKeywords, getProdTecTipos, getProdTecMap } from '../../api'
import ChartsOverview from '../../components/charts-overview'
import { ProdTec } from '../../static_texts'

function ProdTecnica() {
    return (
        <ChartsOverview
            title="Produção Técnica"
            getIndex={getProdTec}
            getCount={getProdTecCount}
            getKeywords={getProdTecKeywords}
            getTipos={getProdTecTipos}
            getMap={getProdTecMap}
            description={ProdTec}
        />
    )
}

export default ProdTecnica