import React from 'react'
import Home from './pages/Home'
import Header from './components/header'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Body } from './styles'
import Menu from './components/menu'
import Footer from './components/footer'
import ProdBibliografica from './pages/ProdBibliografica/ProdBibliografica'
import ProdBibliograficaAUH from './pages/ProdBibliografica/ProdBibliograficaAUH'
import ProdBibliograficaAUP from './pages/ProdBibliografica/ProdBibliograficaAUP'
import ProdBibliograficaAUT from './pages/ProdBibliografica/ProdBibliograficaAUT'
import ProdArtistica from './pages/ProdArtistica/ProdArtistica'
import ProdArtisticaAUH from './pages/ProdArtistica/ProdArtisticaAUH'
import ProdArtisticaAUP from './pages/ProdArtistica/ProdArtisticaAUP'
import ProdArtisticaAUT from './pages/ProdArtistica/ProdArtisticaAUT'
import ProdTecnica from './pages/ProdTecnica/ProdTecnica'
import ProdTecnicaAUH from './pages/ProdTecnica/ProdTecnicaAUH'
import ProdTecnicaAUP from './pages/ProdTecnica/ProdTecnicaAUP'
import ProdTecnicaAUT from './pages/ProdTecnica/ProdTecnicaAUT'
import Orientacoes from './pages/Orientacoes/Orientacoes'
import OrientacoesAUH from './pages/Orientacoes/OrientacoesAUH'
import OrientacoesAUP from './pages/Orientacoes/OrientacoesAUP'
import OrientacoesAUT from './pages/Orientacoes/OrientacoesAUT'
import Bancas from './pages/Bancas/Bancas'
import BancasAUH from './pages/Bancas/BancasAUH'
import BancasAUP from './pages/Bancas/BancasAUP'
import BancasAUT from './pages/Bancas/BancasAUT'
import Premios from './pages/Premios'
import About from './pages/About'
import ScrollToTop from './ScrollToTop'

function App() {
    return (
        <Router onUpdate={() => console.log('oi')}>
            <Header />
            <Menu />
            <Body>
                <React.Fragment>
                    <ScrollToTop />
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route path='/producao-bibliografica' component={ProdBibliografica} />
                        <Route path='/producao-bibliografica-auh' component={ProdBibliograficaAUH} />
                        <Route path='/producao-bibliografica-aup' component={ProdBibliograficaAUP} />
                        <Route path='/producao-bibliografica-aut' component={ProdBibliograficaAUT} />
                        <Route path='/producao-artistica' component={ProdArtistica} />
                        <Route path='/producao-artistica-auh' component={ProdArtisticaAUH} />
                        <Route path='/producao-artistica-aup' component={ProdArtisticaAUP} />
                        <Route path='/producao-artistica-aut' component={ProdArtisticaAUT} />
                        <Route path='/producao-tecnica' component={ProdTecnica} />
                        <Route path='/producao-tecnica-auh' component={ProdTecnicaAUH} />
                        <Route path='/producao-tecnica-aup' component={ProdTecnicaAUP} />
                        <Route path='/producao-tecnica-aut' component={ProdTecnicaAUT} />
                        <Route path='/bancas' component={Bancas} />
                        <Route path='/bancas-auh' component={BancasAUH} />
                        <Route path='/bancas-aup' component={BancasAUP} />
                        <Route path='/bancas-aut' component={BancasAUT} />
                        <Route path='/orientacoes' component={Orientacoes} />
                        <Route path='/orientacoes-auh' component={OrientacoesAUH} />
                        <Route path='/orientacoes-aup' component={OrientacoesAUP} />
                        <Route path='/orientacoes-aut' component={OrientacoesAUT} />
                        <Route path='/premios' component={Premios} />
                        <Route path='/sobre-nos' component={About} />
                    </Switch>
                </React.Fragment>
            </Body>
            <Footer />
        </Router>
    )
}

export default App