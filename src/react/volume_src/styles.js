import styled from 'styled-components'

const Grid = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 64px;
`
const CenteredColumn = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100%;
`
const Body = styled.div`
    margin-top: 208px;
    min-height: calc(100vh - 208px);
    font-family: 'Source Sans Pro', sans-serif;
`
const Row = styled.div`
    display: flex;
    > div:not(:last-child) {
        margin-right: 64px;
    }
    margin-bottom: 32px;
`
const TextBox = styled.div`
    width: calc(100% - 48px);
    background-color: #D1D1D1;
    padding: 16px 24px 16px 24px;
    display: flex;
    align-items: flex-start;
    flex-direction: column;
    margin-top: 16px;
    > span {
        font-size: 22px;
        opacity: 0.8;
        :not(:last-child) {
            margin-bottom: 24px;
        }
    }
`
const TextArea = styled.div`
    width: 100%;
    max-width: 1280px;
`
const Title = styled.span`
    font-size: 48px;
    padding-bottom: 32px;
`
const SubTitle = styled.span`
    font-size: 28px;
    margin: 32px 0px 32px;
`

const primary = '#383838'
const secondary = '#878787'
const departmentColors = {
    AUH: '#e3d83b',
    AUT: '#2eb9d9',
    AUP: '#e01b2b'
}
const categoriesColors = [
    '#b8730b',
    '#0c7a00',
    '#42ffb7',
    '#0f40d4',
    '#97a0de',
    '#b017e8',
    '#f07c1d',
    '#997fdb',
    '#69283f',
    '#778078',
    '#998f87',
    '#b5b5b5',
    '#2e0202',
    '#235f70',
    '#a0ab3c'
]
export {
    Grid,
    CenteredColumn,
    Body,
    Row,
    primary,
    secondary,
    TextBox,
    TextArea,
    departmentColors,
    Title,
    SubTitle,
    categoriesColors
}