## Cliente do projeto


### Instalar dependências

Certifique-se que você possui o gerenciador de pacotes **yarn**. Assim, para instalar as dependências do front-end, basta executar o comando:

`yarn`


### Executar projeto

Uma vez as dependências instaladas, execute o seguinte comando para iniciar uma versão de desenvolvimento:

`yarn start`

O projeto rodará em **localhost:3000** e toda mudança feita no código refletirá instantaneamente em seu navegador (hot reload).



