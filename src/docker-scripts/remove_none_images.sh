#!/bin/bash

#Deleta todas as imagens não usadas do docker (para salvar espaço).

docker rmi $(docker images --filter "dangling=true" -q)