#!/bin/bash

#Deleta os arquivos do postgres. Usado para criar uma database nova.
#USE COM CUIDADO!

help(){
  echo "Deleta os arquivos do postgres. Usado para criar uma database nova."
  echo "USE COM CUIDADO!"
  echo "Uso: delete_database"
  echo -e "-h/--help \t Mostra instruções de uso."
  echo -e "-y/--yes \t Automaticamente responde \"Y\" para todas as perguntas."
  echo -e "-nup/--no-up \t Não realiza o docker-compose up/down."
  exit
}

get_reply(){
  while :; do
    read -p "$MESSAGE" -n 1 -r
    echo
    case $REPLY in
      Y|y|S|s)
        REPLY="T"
        break
        ;;

      N|n)
        REPLY="F"
        break
        ;;

      *)
        echo "Resposta inválida."

    esac
  done
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
UP="T"
YES="F"
STARTED="F"
QUESTION=""

while :; do
  case $1 in
    -h|--help)
      help
      ;;

    -nup|--no-up)
      UP="F"
      ;;

    -y|--yes)
      YES="T"
      break
      ;;

    -?*)
      echo "Comando não reconhecido: $1."
      help
      ;;

    *)
      break
      
  esac
  shift
done

MESSAGE="Esse script vai deletar todos os dados do postgres. Prosseguir? (y/n) "
get_reply

if [[ $REPLY == "T" ]]
then
  cd "$DIR"/../postgres
  rm -r volume_db-data

  cd "$DIR"

  if [[ "$YES" == "F" ]]; then
    MESSAGE="Aplicar migrações? (y/n) "
    get_reply
  fi

  if [[ "$YES" == "T" ]] || [[ $REPLY == "T" ]];then

    if [[ "$UP" == "T" ]];then
      ./docker-up.sh db web
      STARTED="T"
    fi

    ./migrate_django.sh -nup
  fi

  if [[ "$YES" == "F" ]]; then
    MESSAGE="Repopular o banco? (y/n) "
    get_reply
  fi

  if [[ "$YES" == "T" ]] || [[ $REPLY == "T" ]];then

    if [[ "$UP" == "T" ]];then
      ./docker-up.sh db web
      STARTED="T"
    fi

    ./populate_database.sh -nup -v

  fi

  if [[ "$STARTED" == "T" ]];then
    ./docker-up.sh -d
  fi

fi