#!/bin/bash

#Da docker-compose up. Ou down, caso a opção -down seja escolhida

help(){
  echo "Faz docker-compose up. Usado para unificar o serviço entre scripts."
  echo "Uso: docker-up [ARGS] <serviços>"
  echo -e "-h/--help \t\t Mostra instruções de uso."
  echo -e "-t/--time <tempo> \t Muda o tempo de espera para <tempo>."
  echo -e "-d/--down \t faz docker-compose down."
  exit
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
TIME="30"
DOWN="F"
STARTED="F"

SERVS=()

while :; do
  case $1 in
    -h|--help)
      help
      ;;

    -d|--down)
      DOWN="T"
      break
      ;;

    -?*)
      echo "Comando não reconhecido: $1."
      help
      ;;

    ?*)
      SERVS+=("$1")
      ;;

    *)
      break
      
  esac
  shift
done

cd "$DIR"/..


if [[ "$DOWN" == "T" ]]; then
  docker-compose down

elif [[ "${#SERVS[@]}" == 0 ]]; then
  docker-compose up -d
  sleep "$TIME"

else 
  for serv in "${SERVS[@]}"; do

    if [ -z `docker-compose ps -q "$serv"` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q "$serv")` ]; then
      docker-compose up -d "$serv"
      STARTED="T"
    # else
    #   echo "Serviço $serv já está rodando!"
    fi

  done

  if [[ "$STARTED" == "T" ]]; then
    sleep "$TIME"
  fi

fi
