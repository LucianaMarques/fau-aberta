#!/bin/bash

#Realiza as migrações do banco no django.

help(){
  echo "Realiza as migrações do banco de dados dentro do django."
  echo "Uso: migrate_django"
  echo -e "-h/--help \t Mostra instruções de uso."
  echo -e "-nup/--no-up \t Não realiza o docker-compose up/down."
  exit
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
UP="T"

while :; do
  case $1 in
    -h|--help)
      help
      ;;

    -nup|--no-up)
      UP="F"
      ;;

    -?*)
      echo "Comando não reconhecido: $1."
      help
      ;;

    *)
      break
      
  esac
  shift
done

if [ "$UP" == "T" ];then
  cd "$DIR"/
  ./docker-up.sh db web
fi

docker exec -it fauserver_django python3 manage.py migrate

if [ "$UP" == "T" ];then
  ./docker-up.sh -d
fi