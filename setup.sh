#!/bin/bash

#Prepara o ambiente de desenvolvimento. Rode somente uma vez.

help(){
  echo "Prepara o ambiente de desenvolvimento."
  echo "Rode somente uma vez!"
  echo "Uso: setup"
  echo -e "-h/--help \t Mostra instruções de uso."
  echo -e "-y/--yes \t Automaticamente responde \"Y\" para todas as perguntas."
  exit
}

get_reply(){
  while :; do
    read -p "$MESSAGE" -n 1 -r
    echo
    case $REPLY in
      Y|y|S|s)
        REPLY="T"
        break
        ;;

      N|n)
        REPLY="F"
        break
        ;;

      *)
        echo "Resposta inválida."

    esac
  done
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
YES="F"

while :; do
  case $1 in
    -h|--help)
      help
      ;;

    -y|--yes)
      YES="T"
      break
      ;;

    -?*)
      echo "Comando não reconhecido: $1."
      help
      ;;

    *)
      break
      
  esac
  shift
done

cd "$DIR"/src/docker-scripts/

if [[ $YES == "F" ]]; then
  echo "Esse script deve ser rodado só uma vez, no inicio do projeto."
  MESSAGE="Deseja prosseguir? (y/n) "
  get_reply
fi

if [[ $YES == "T" ]] || [[ $REPLY == "T" ]];then

  echo "Montando as imagens..."
  ./build_image.sh

  echo "Abrindo docker..."
  ./docker-up.sh

  echo "Migrando Django..."
  ./migrate_django.sh -nup

  if [[ $YES == "F" ]]; then
    MESSAGE="Popular banco? (y/n) "
    get_reply
  fi

  if [[ $YES == "T" ]] || [[ $REPLY == "T" ]];then
      ./populate_database.sh -nup
  fi

  if [[ $YES == "F" ]]; then
    MESSAGE="Fechar o docker? (y/n) "
    get_reply
  fi

  if [[ $YES == "T" ]] || [[ $REPLY == "T" ]];then
      ./docker-up.sh -d
  fi

  echo "Finalizado."

fi